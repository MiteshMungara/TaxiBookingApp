//
//  TheJeeTexiStoryViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "TheJeeTexiStoryViewController.h"
#import "MainPageViewController.h"
#import "SidebarViewController.h"
@interface TheJeeTexiStoryViewController ()
@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation TheJeeTexiStoryViewController
@synthesize lbl_story1,lbl_story2;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Scroll.contentSize = CGSizeMake(320, 750);
    
    /*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [panGesture delaysTouchesBegan];
    [self.view addGestureRecognizer:panGesture];*/
    self.sidebarVC = [[SidebarViewController alloc] init];
    [self.sidebarVC setBgRGB:0x000000];
    self.sidebarVC.view.frame  = self.view.bounds;
    [self.view addSubview:self.sidebarVC.view];
         
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadDataJeeTexi) userInfo: nil repeats: NO];
}

-(void)loadDataJeeTexi
{
    lbl_story1.text = @"      In the month of May 2015 , a man came to Delhi from another city to give an interview. It was a hot sunny day with scorching heat. Since the journey was not planned ,no return tickets were made.After the inverview was over it was nearly 4 p.m. While returning back to his home town he thought he can spare 3-4 thousands on a taxi to go back to his town. He could not afford flight as the fare was too high.Train reservation on the spot was out of question.This person thought of calling his friend in Delhi who was running a Online portal offering hotel and car bookings.This person asked his friend that he needs a taxi from Delhi to Jaipur.To this his friend replied & It can be done but it will take nearly 4 hours.Oh ! what the ? Ultimately he took a state transport bus from Iffco Chowk and travelled sweating the whole way.";
    lbl_story1.numberOfLines = 0;
    lbl_story1.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(lbl_story1.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [lbl_story1 sizeThatFits:maximumLabelSize];
    lbl_story1.frame = CGRectMake(lbl_story1.frame.origin.x, lbl_story1.frame.origin.y, expectSize.width, expectSize.height);
    
    lbl_story2.text = @"           On doing research , the man was surprised to know that till 2015 more than Rs 600 crores have been invested in Indian taxi market but still 88 % of market is unorgainzed. Further it was found that the industry was growing at the rate of 20 % (as per Valoriser Consultants).The man shared this story with my two friends.They took the pain and the labor resulted in birth of TaxiPAPA - the only branded online taxi offering intercity taxis in North India.";
    lbl_story2.numberOfLines = 0;
    lbl_story2.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize2 = CGSizeMake(lbl_story2.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize2 = [lbl_story2 sizeThatFits:maximumLabelSize2];
    lbl_story2.frame = CGRectMake(lbl_story2.frame.origin.x, lbl_story1.frame.size.height+lbl_story1.frame.origin.y+10, expectSize2.width, expectSize2.height);
}

- (IBAction)show:(id)sender
{
    [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
    [self.sidebarVC panDetected:recoginzer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
