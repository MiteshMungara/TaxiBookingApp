//
//  ContactUSViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "AppDelegate.h"

@interface ContactUSViewController : UIViewController
{
    AppDelegate *appDel;
    CGFloat animateDistance;

}
@property(strong,nonatomic) IBOutlet UITextField *txtname;;
@property(strong,nonatomic) IBOutlet UITextField *txtphoneno;
@property(strong,nonatomic) IBOutlet UITextField *txtdetail;
@property(strong,nonatomic) IBOutlet UITextField *txtemail;
@property(strong,nonatomic) IBOutlet UITextField *txtcity;

-(IBAction)submitContactBtnPressed:(id)sender;
@end
