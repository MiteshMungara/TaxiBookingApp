//
//  SidebarViewController.m
//  LLBlurSidebar
//
//  Created by Lugede on 14/11/20.
//  Copyright (c) 2014年 lugede.cn. All rights reserved.
//

#import "SidebarViewController.h"
#import "MainPageViewController.h"
#import "TheJeeTexiStoryViewController.h"
#import "AboutUsViewController.h"
#import "ContactUSViewController.h"
#import "ListRouteViewController.h"
#import "ListOfDriverBidViewController.h"
#import "ManagedBookingViewController.h"
@interface SidebarViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *titles;
    NSArray *images;
}
@property (nonatomic, retain) UITableView* menuTableView;

@end

@implementation SidebarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = [UIApplication sharedApplication].delegate;
    
    //MenuNameArr = [[NSMutableArray alloc]initWithObjects:@"Search",@"Filter",@"Featured",@"My trips",@"Sign up",@"Help", nil];
    
    self.menuTableView = [[UITableView alloc] initWithFrame:self.contentView.bounds];
    self.menuTableView.separatorColor=[UIColor clearColor];
    //[self.menuTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.menuTableView.backgroundColor = [UIColor clearColor];
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    
    self.menuTableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 62.0f)];
        view.backgroundColor=[UIColor clearColor];
        UIButton *MenuCloseButton = [[UIButton alloc] initWithFrame:CGRectMake(195, 25, 35, 35)];
        MenuCloseButton.backgroundColor = [UIColor clearColor];
        [MenuCloseButton addTarget:self action:@selector(CloseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        UIImageView *line1 = [[UIImageView alloc] initWithFrame:CGRectMake(195, 25, 35, 35)];
        line1.image = [UIImage imageNamed:@"menu_icon.png"];
        //line1.backgroundColor = [UIColor blackColor];
        [view addSubview:line1];
        [view addSubview:MenuCloseButton];
        
        view;
    });
    
    [self.contentView addSubview:self.menuTableView];
    
    titles = [NSArray arrayWithObjects:@"Home",@"Booking History",@"Cancel Booking",@"Driver Bids",@"About Us",@"The TaxiPapa Story",@"Contact Us",@"Rate This App", nil];
    images = [NSArray arrayWithObjects:@"home_icon.png",@"history.png",@"cancle.png",@"driverbid.png",@"taxi_story.png",@"contact_us.png", @"about_us.png",@"rating.png",nil];
}
-(void)CloseButtonClicked:sender
{
    [self showHideSidebar];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return titles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sidebarMenuCellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sidebarMenuCellIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sidebarMenuCellIdentifier] ;
        cell.backgroundColor = [UIColor clearColor];
    }
    UIView *bgColorView = [[UIView alloc] init];
    //.backgroundColor =
    
    [bgColorView setBackgroundColor:[UIColor colorWithRed:57/255.0 green:181/255.0 blue:74/255.0 alpha:1]];
     //[UIColor colorWithRed:57/255 green:181/255 blue:74/255 alpha:1]];
    //[UIColor colorWithRed:253/255.0 green:233/255.0 blue:71/255.0 alpha:1]

    //bgColorView.backgroundColor = [UIColor yellowColor];
    [cell setSelectedBackgroundView:bgColorView];
    cell.textLabel.textColor = [UIColor blackColor];
    if(indexPath.row != images.count){
        UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, 320, 1)];
        line.backgroundColor = [UIColor blackColor];
        [cell addSubview:line];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [titles objectAtIndex:indexPath.row]];
    
    [cell.textLabel setFont: [cell.textLabel.font fontWithSize: 15]];
    cell.textLabel.textColor = [UIColor colorWithRed:38/255.0 green:34/255.0 blue:98/255.0 alpha:1];
    cell.imageView.image = [UIImage imageNamed:[images objectAtIndex:indexPath.row]];
   
    //  self.menuTableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0) {
         UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        MainPageViewController *MainPage = (MainPageViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
        [navigationController pushViewController:MainPage animated:YES];
        
    }
    else if (indexPath.row==1) {
        
        UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        ListRouteViewController *Listroute = (ListRouteViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListRouteViewController"];
        [navigationController pushViewController:Listroute animated:YES];
        
        
    }
    else if (indexPath.row==2) {
        
        UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        ManagedBookingViewController *book = (ManagedBookingViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ManagedBookingViewController"];
        [navigationController pushViewController:book animated:YES];
        
        
    }
    else if (indexPath.row==3) {
        
        UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        ListOfDriverBidViewController *about = (ListOfDriverBidViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListOfDriverBidViewController"];
        [navigationController pushViewController:about animated:YES];
        
        
    }
    else if (indexPath.row==4) {
        
        UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        AboutUsViewController *about = (AboutUsViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"AboutUsViewController"];
        [navigationController pushViewController:about animated:YES];

        
    }
    else if (indexPath.row==5) {
        
        
        UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        TheJeeTexiStoryViewController *HelpPe = (TheJeeTexiStoryViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"TheJeeTexiStoryViewController"];
        [navigationController pushViewController:HelpPe animated:YES];
        
    }
    else if (indexPath.row==6) {
        
        UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ContactUSViewController *ContactUS = (ContactUSViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ContactUSViewController"];
        [navigationController pushViewController:ContactUS animated:YES];
        
        
    }
    else if (indexPath.row==7) {
         [self showHideSidebar];
        
       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/cab77/id1076902432?ls=1&mt=8"]];
        [self.menuTableView reloadData];
      //  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];
        //ContactUSViewController *ContactUS = (ContactUSViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ContactUSViewController"];
       // [self.navigationController pushViewController:ContactUS animated:YES];
        
    }
     [self showHideSidebar];
}
@end
