//
//  AppDelegate.m
//  JeeTexiApp
//
//  Created by iSquare5 on 01/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.

//

#import "AppDelegate.h"
#import "MainPageViewController.h"
#import "ListOfDriverBidViewController.h"
#import "ViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize confirmBook,useridStr,urlStr;
@synthesize deviceToken,deviceTokenStr,deviceTokenuser,openBidStr,pushNotificationArr;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    sleep(5);
   confirmBook=@"no";
    NSString *push;
    push=@"0";
    
    urlStr = @"http://www.smartbaba.in/taxipapa/";
    
    //pushnotification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
    }
    
    if (launchOptions != nil)
    {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"You received push notification" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alertView show];
            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            [navigationController setNavigationBarHidden:YES animated:YES];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            ListOfDriverBidViewController *inboxpage = (ListOfDriverBidViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListOfDriverBidViewController"];
            [navigationController pushViewController:inboxpage animated:NO];
        }
        else
        {
           
        }
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *myString = [prefs stringForKey:@"idKey"];
    
    //    [prefs setInteger:app.userid forKey:@"idKey"];
    
    if([myString length] != 0)
    {
        NSLog(@"%ld",(long)myString);
        useridStr = myString;
        NSLog(@"%@",useridStr);
        
        UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        MainPageViewController *Main = (MainPageViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
        [navigationController pushViewController:Main animated:NO];
    }
    else
    {
        
        UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        ViewController *View = (ViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ViewController"];
        [navigationController pushViewController:View animated:YES];
    }
   
    
    return YES;
    
}


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
// Failed to send a message



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceTokena {
    
    deviceTokenStr=[NSString stringWithFormat:@"%@",deviceTokena];
    deviceTokenStr=[deviceTokenStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceTokenStr=[deviceTokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceTokenStr=[deviceTokenStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    deviceTokenuser=[NSString stringWithFormat:@"%@",deviceTokenStr];
    NSLog(@"deviceTokenStr :%@",deviceTokenuser);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //[PFPush handlePush:userInfo];
     pushNotificationArr = [userInfo valueForKey:@"aps"];
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground ||
       [UIApplication sharedApplication].applicationState == UIApplicationStateInactive)
    {
       
        
    if ([[pushNotificationArr valueForKey:@"alert"] containsString:@"Get bid from"])
    {
          if ([openBidStr isEqualToString:@"Yes"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshTableOFPUSHnoti" object:nil];
    
        }
        else
        {
             UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            [navigationController setNavigationBarHidden:YES animated:YES];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            ListOfDriverBidViewController *inboxpage = (ListOfDriverBidViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListOfDriverBidViewController"];
            [navigationController pushViewController:inboxpage animated:NO];
        }
        
    }
        
    }else{
        
    if ([[pushNotificationArr valueForKey:@"alert"] containsString:@"Get bid from"])
    {
        if ([openBidStr isEqualToString:@"Yes"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshTableOFPUSHnoti" object:nil];
            
        }
        else
        {
            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            [navigationController setNavigationBarHidden:YES animated:YES];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            ListOfDriverBidViewController *inboxpage = (ListOfDriverBidViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListOfDriverBidViewController"];
            [navigationController pushViewController:inboxpage animated:NO];
        }
    }
}

    NSLog(@"Notification %@",userInfo);

    //[[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:userInfo];
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
