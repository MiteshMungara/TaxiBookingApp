//
//  MainPageViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//


#import "MainPageViewController.h"
#import "SidebarViewController.h"
#import "NIDropDown.h"
#import "Reachability.h"
#import "MVPlaceSearchTextField.h"
#import "ListOfDriverBidViewController.h"
#import "Reachability.h"
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 280;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
@interface MainPageViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
   
    NSMutableArray *cityNameArray;
    NSArray *tempFromCityNamearr;
    NSArray *tempToCityNamearr;
    NSArray *cityIdArray;
  
    NSArray *fromcityareaArray;
    NSArray *tocityareaArray;
    NSArray *typecarArray;
    NSArray *typecaridArray;
    NSString *saveCarId;
    NSMutableArray *rsArr;
    NSString *checktextfield;
    NSArray *fromlocation;
    NSArray *tolocation;
    NSString *selecttimeBid;
    NSString *textWriteIdentify;
    NSString *tocityIdStr;
    NSString *fromcityIdStr;
    UITableView *autoCompleteTableView;
    NSMutableArray *autoCompleteArray2;
    NSArray *cityIdArray2;
    NSDate *startDate;
    NSDate *endDate;
    NSString *daystr;
}
//@property (strong, nonatomic) IBOutlet UITableView *autoCompleteTableView;
@property (nonatomic, retain) NSMutableArray *autoCompleteArray;
@property (nonatomic, retain) NSArray *autoCompleteFilterArray;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtFromPlaceSearch;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtToPlaceSearch;
@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation MainPageViewController
@synthesize txttotalday,txtfromcity,txtfromcityarea,txttocity,txttocityarea,txtpickupdate,txtpickuptime,txtname,txtemail,txtcar,dropdowndaysradioimg,txtFromPlaceSearch,txtToPlaceSearch;
@synthesize autoCompleteArray;
@synthesize autoCompleteFilterArray;


- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];

}


-(void)viewDidAppear:(BOOL)animated{
    
    daystr = @"OneWay";
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [hud show:YES];
     [NSTimer scheduledTimerWithTimeInterval: 0.8 target: self
                                             selector: @selector(loadDataMail) userInfo: nil repeats: NO];
}

#pragma mark - Load Data
-(void)loadDataMail
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        
        // Do any additional setup after loading the view.
        Scroll.contentSize = CGSizeMake(320, 535);
        _onewayradioimg.image = [UIImage imageNamed:@"redio-check.png"];
        onewayagreecheckButton.selected=YES;
        autoCompleteArray = [[NSMutableArray alloc]init];
        autoCompleteArray2 = [[NSMutableArray alloc]init];
        dropdowndaysradioimg.hidden=YES;
        dropDownOfDaysButton.hidden=YES;
        txttotalday.hidden=YES;
        /*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
        [panGesture delaysTouchesBegan];
        [self.view addGestureRecognizer:panGesture];*/
        self.sidebarVC = [[SidebarViewController alloc] init];
        [self.sidebarVC setBgRGB:0x000000];
        self.sidebarVC.view.frame  = self.view.bounds;
        [self.view addSubview:self.sidebarVC.view];
        
        cityNameArray = [[NSMutableArray alloc]init];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtfromcity.leftView = paddingView;
        txtfromcity.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtfromcityarea.leftView = paddingView2;
        txtfromcityarea.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txttocity.leftView = paddingView3;
        txttocity.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txttocityarea.leftView = paddingView4;
        txttocityarea.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtpickupdate.leftView = paddingView5;
        txtpickupdate.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtpickuptime.leftView = paddingView6;
        txtpickuptime.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtname.leftView = paddingView7;
        txtname.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtemail.leftView = paddingView8;
        txtemail.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txtcar.leftView = paddingView9;
        txtcar.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView10 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        txttotalday.leftView = paddingView10;
        txttotalday.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView11 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        _txtReturnDate.leftView = paddingView11;
        _txtReturnDate.leftViewMode = UITextFieldViewModeAlways;
        
        rsArr = [[NSMutableArray alloc]init];
        
        for (int i=2; i<11; i++) {
            [rsArr addObject:[NSString stringWithFormat:@"%d day",i]];
        }
        [txtfromcity addTarget:self action:@selector(AutofillFromCitytextfill:) forControlEvents:UIControlEventEditingChanged];
        [txttocity addTarget:self action:@selector(AutofillTooCitytextfill:) forControlEvents:UIControlEventEditingChanged];
        
        // [txtfromcity addTarget:self action:@selector(F1) forControlEvents:UIControlEventEditingChanged];
        //  [txttocity addTarget:self action:@selector(T1) forControlEvents:UIControlEventEditingChanged];
        
        [self getCityRecordArray];
        [self getcarrecordarray];
        
        
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 375, 100)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.barTintColor = [UIColor blackColor];
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
        [numberToolbar sizeToFit];
        
        txtpickupdate.inputAccessoryView = numberToolbar;
        txtpickuptime.inputAccessoryView = numberToolbar;
        txttotalday.inputAccessoryView = numberToolbar;
        txtfromcity.inputAccessoryView = numberToolbar;
        txttocity.inputAccessoryView = numberToolbar;
        txttocityarea.inputAccessoryView = numberToolbar;
        txtfromcityarea.inputAccessoryView = numberToolbar;
        txtpickupdate.inputAccessoryView = numberToolbar;
        txtpickuptime.inputAccessoryView = numberToolbar;
        txtname.inputAccessoryView = numberToolbar;
        txtemail.inputAccessoryView = numberToolbar;
        _txtReturnDate.inputAccessoryView = numberToolbar;
        
        datePicker =  [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 185)];
        [datePicker setDate:[NSDate date]];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(updateOnTextField:) forControlEvents:UIControlEventValueChanged];
        [txtpickupdate setInputView:datePicker];
        
         returndatePicker =  [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 185)];
        [returndatePicker setDate:[NSDate date]];
        returndatePicker.datePickerMode = UIDatePickerModeDate;
        [returndatePicker addTarget:self action:@selector(updatereturnOnTextField:) forControlEvents:UIControlEventValueChanged];
        [_txtReturnDate setInputView:returndatePicker];
        
        //Time picker
        timePicker =  [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 185)];
        timePicker.datePickerMode = UIDatePickerModeTime;
        timePicker.timeZone = [NSTimeZone defaultTimeZone];
        timePicker.minuteInterval = 5;
        [timePicker addTarget:self action:@selector(showTimeOfTextField:) forControlEvents:UIControlEventValueChanged];
        [txtpickuptime setInputView:timePicker];
        [hud hide:YES];
    }
}
#pragma mark - Text Field Date,Time
-(void)updateOnTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)txtpickupdate.inputView;
    // dateTextOfScan.text = [NSString stringWithFormat:@"%@",picker.date];
    [picker setMinimumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    startDate = picker.date;
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    txtpickupdate.text = [NSString stringWithFormat:@"%@",dateString];
}
-(void)updatereturnOnTextField:(id)sender
{
    int oneDay = 1;
    UIDatePicker *Returnpicker = (UIDatePicker*)_txtReturnDate.inputView;
    NSDate *maxDateFromBeginig = [startDate dateByAddingTimeInterval:60*60*24*oneDay]; //24h + beginDate
    //NSDate *minDateFromBeginig = [NSDate dateWithTimeInterval:1.0 sinceDate:Returnpicker.date]; //1 sec + beginDate
    
    // dateTextOfScan.text = [NSString stringWithFormat:@"%@",picker.date];
    [Returnpicker setMinimumDate:maxDateFromBeginig];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = Returnpicker.date;
    endDate = Returnpicker.date;
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _txtReturnDate.text = [NSString stringWithFormat:@"%@",dateString];
}

-(void)showTimeOfTextField:(id)sender
{
    
    UIDatePicker *picker = (UIDatePicker*)txtpickuptime.inputView;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    picker.datePickerMode = UIDatePickerModeTime;
    picker.timeZone = [NSTimeZone defaultTimeZone];
    [dateFormat setDateFormat:@"HH:mm"];
    txtpickuptime.text = [dateFormat stringFromDate:picker.date];
}
-(IBAction)ReturnDateBtnPressed:(id)sender
{
    [_txtReturnDate becomeFirstResponder];
}
-(IBAction)dateBtnClicked:(id)sender
{
    [txtpickupdate becomeFirstResponder];
}
-(IBAction)timeBtnClicked:(id)sender
{
    [txtpickuptime becomeFirstResponder];
}
#pragma mark - Time for Bid Selection
-(IBAction)checkBidthirtyTimeBtnPressed:(id)sender
{
  //  radiofiftyminButton.selected=NO;
    radiofourtyfiveminButton.selected=NO;
    radioonehourButton.selected=NO;
    radiothirtyminButton.selected=YES;
    //_fiftyradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _thiredradioimg.image = [UIImage imageNamed:@"redio-check.png"];
    _fourthradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _onehourradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    selecttimeBid = @"15";
}
-(IBAction)checkBidfourtyfiveTimeBtnPressed:(id)sender
{
   // radiofiftyminButton.selected=NO;
    radiofourtyfiveminButton.selected=YES;
    radioonehourButton.selected=NO;
    radiothirtyminButton.selected=NO;
    //.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _thiredradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _fourthradioimg.image = [UIImage imageNamed:@"redio-check.png"];
    _onehourradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    selecttimeBid = @"30";
}
-(IBAction)checkBidonehourTimeBtnPressed:(id)sender
{
  //  radiofiftyminButton.selected=NO;
    radiofourtyfiveminButton.selected=NO;
    radioonehourButton.selected=YES;
    radiothirtyminButton.selected=NO;
   // _fiftyradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _thiredradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _fourthradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _onehourradioimg.image = [UIImage imageNamed:@"redio-check.png"];
    selecttimeBid = @"45";
}


-(void)cancelNumberPad{
    if ([txtpickupdate resignFirstResponder])
    {
        [txtpickupdate resignFirstResponder];
        txtpickupdate.text = @"";
    }
    if ([txtpickuptime resignFirstResponder])
    {
        [txtpickuptime resignFirstResponder];
        txtpickuptime.text = @"";
    }
    if ([txttotalday resignFirstResponder])
    {
        [txttotalday resignFirstResponder];
        txttotalday.text = @"";
    }
    if ([txtfromcity resignFirstResponder])
    {
        [txtfromcity resignFirstResponder];
        txtfromcity.text = @"";
    }
    if ([txttocity resignFirstResponder])
    {
        [txttocity resignFirstResponder];
        txttocity.text = @"";
    }
    if ([txttocityarea resignFirstResponder])
    {
        [txttocityarea resignFirstResponder];
        txttocityarea.text = @"";
    }
    if ([txtfromcityarea resignFirstResponder])
    {
        [txtfromcityarea resignFirstResponder];
        txtfromcityarea.text = @"";
    }
    if ([txtpickupdate resignFirstResponder])
    {
        [txtpickupdate resignFirstResponder];
        txtpickupdate.text = @"";
    }
    if ([_txtReturnDate resignFirstResponder])
    {
        [_txtReturnDate resignFirstResponder];
        _txtReturnDate.text = @"";
    }
    if ([txtpickuptime resignFirstResponder])
    {
        [txtpickuptime resignFirstResponder];
        txtpickuptime.text = @"";
    }
    if ([txtname resignFirstResponder])
    {
        [txtname resignFirstResponder];
        txtname.text = @"";
    }
    if ([txtemail resignFirstResponder])
    {
        [txtemail resignFirstResponder];
        txtemail.text = @"";
    }
}

-(void)doneWithNumberPad
{
   NSString *numbertotalTheKeyboard = txttotalday.text;
    
    if (![numbertotalTheKeyboard isEqualToString:@""]) {
        numbertotalTheKeyboard = txttotalday.text;
        [txtfromcity becomeFirstResponder];
    }
    
    if (roundtripcheckButton.selected == NO) {
        NSString *numberFromTheKeyboard = txtpickupdate.text;
        if (![numberFromTheKeyboard isEqualToString:@""]) {
            [txtpickupdate resignFirstResponder];
            [txtpickuptime becomeFirstResponder];
        }
    }
    else
    {
        NSString *numberFromTheKeyboard = txtpickupdate.text;
        if (![numberFromTheKeyboard isEqualToString:@""]) {
            [txtpickupdate resignFirstResponder];
            [_txtReturnDate becomeFirstResponder];
        }
    }
    
    NSString *numberDateTheKeyboard = _txtReturnDate.text;
    if (![numberDateTheKeyboard isEqualToString:@""]) {
        [_txtReturnDate resignFirstResponder];
        [txtpickuptime becomeFirstResponder];
    }
    NSString *numberToTheKeyboard = txtpickuptime.text;
    
    if (![numberToTheKeyboard isEqualToString:@""]) {
        [txtpickuptime resignFirstResponder];
        [txtname becomeFirstResponder];
    }
    NSString *numberToTheKeyboardfrom = txtfromcity.text;
    
    if (![numberToTheKeyboardfrom isEqualToString:@""]) {
        [txtfromcity resignFirstResponder];
    }
    NSString *numberToTheKeyboardto = txttocity.text;
    
    if (![numberToTheKeyboardto isEqualToString:@""]) {
        [txttocity resignFirstResponder];
    }
    NSString *numberToTheKeyboardfromarea = txtfromcityarea.text;
    
    if (![numberToTheKeyboardfromarea isEqualToString:@""]) {
        [txtfromcityarea resignFirstResponder];
    }
    NSString *numberToTheKeyboardtoarea = txttocityarea.text;
    
    if (![numberToTheKeyboardtoarea isEqualToString:@""]) {
        [txttocityarea resignFirstResponder];
    }
    NSString *numberToTheKeyboardemail = txtemail.text;
    
    if (![numberToTheKeyboardemail isEqualToString:@""]) {
        [txtemail resignFirstResponder];
    }
    NSString *numberToTheKeyboardname = txtname.text;
    
    if (![numberToTheKeyboardname isEqualToString:@""]) {
        [txtname resignFirstResponder];
    }
   
}

#pragma mark - Get City Record
-(void)getCityRecordArray
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        
    [dropDown closeDropDown];
    dropDown=nil;
      
        
        NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/city.php",appDel.urlStr];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL: url];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response,
                                                    NSData *data,
                                                    NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 
                 NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                 
                 NSLog(@"whaole__%@",jsonDictionary);
                 NSString *successtr = [jsonDictionary valueForKey:@"Success"] ;
                 NSInteger success = [successtr integerValue];
                 
                 if(success == 1)
                 {
                    
                     NSArray *tempfromcityarr=[[jsonDictionary valueForKey:@"poststo"]valueForKey:@"city_name"];
                     for (int i=0; i<tempfromcityarr.count; i++) {
                         [autoCompleteArray addObject:[tempfromcityarr objectAtIndex:i]];
                     }
                     cityIdArray=[[jsonDictionary valueForKey:@"poststo"]valueForKey:@"id"];
                     
                     NSArray *tempTocityarr=[[jsonDictionary valueForKey:@"posts"]valueForKey:@"city_name"];
                     for (int i=0; i<tempTocityarr.count; i++) {
                         [autoCompleteArray2 addObject:[tempTocityarr objectAtIndex:i]];
                     }
                     cityIdArray2=[[jsonDictionary valueForKey:@"posts"]valueForKey:@"id"];
                 }
                 
             }
             
         }];


    }
    
}

#pragma mark - Get Car Record
-(void)getcarrecordarray
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        
          NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/car.php",appDel.urlStr];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL: url];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response,
                                                    NSData *data,
                                                    NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 
                 NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                 
                 NSLog(@"whaole__%@",jsonDictionary);
                 NSString *successtr = [jsonDictionary valueForKey:@"Success"] ;
                 NSInteger success = [successtr integerValue];
                 
                 if(success == 1)
                 {
                     typecarArray = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_name"];
                     typecaridArray = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"id"];
                 }
                 
             }
             
         }];
    }
}


#pragma mark - Drop Down Button
#pragma mark -- From City Data Dropdown
-(void)AutofillFromCitytextfill:(id)sender
{
    [dropDown closeDropDown];
    dropDown=nil;

    NSString *fcitystr = txtfromcity.text;
    if (![fcitystr isEqualToString:@""])
    {
    NSString *fromcity = txtfromcity.text;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@",fromcity];
    tempFromCityNamearr = [autoCompleteArray2 filteredArrayUsingPredicate:pred];
        
          CGFloat f=1;
        
        if(dropDown == nil) {
                NSString *deviceType = [UIDevice currentDevice].model;
                NSLog(@"iphone %@",deviceType);
                if([deviceType isEqualToString:@"iPhone"])
                {
                    for (int i=0; i<tempFromCityNamearr.count; i++) {
                        if (f < 95)
                        {
                        f=f+26;
                        }
                    }
                }
                else if([deviceType isEqualToString:@"iPhone Simulator"])
                {
                    for (int i=0; i<tempFromCityNamearr.count; i++) {
                        if (f < 95)
                        {
                        f=f+26;
                        }
                    }
                }
                else
                {
                    for (int i=0; i<tempFromCityNamearr.count; i++) {
                        if (f < 95)
                        {
                        f=f+30;
                        }
                    }
                }
                if (cityNameArray) {
                    dropDown = [[NIDropDown alloc]showDropDown:sender :&f :tempFromCityNamearr :tempFromCityNamearr :@"down"];
                    dropDown.tag=1;
                    dropDown.delegate = self;
                   // dropDownFromCityButton.selected=YES;
                }
            
            }
            else {
                [dropDown hideDropDown:sender];
                [self rel];
            }
            [self pickerDismiss];
    }
    else
    {
        [dropDown closeDropDown];
        [self rel];
    }
    
}

#pragma mark -- To City Dropdown
-(void)AutofillTooCitytextfill:(id)sender
{
    [dropDown closeDropDown];
    dropDown=nil;
    NSString *tcitystr = txttocity.text;
    if (![tcitystr isEqualToString:@""])
    {
        NSString *tocity = txttocity.text;
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@",tocity];
        tempToCityNamearr = [autoCompleteArray filteredArrayUsingPredicate:pred];
        
        
        NSString *deviceType = [UIDevice currentDevice].model;
        if(dropDown == nil) {
            CGFloat f=1;
            if([deviceType isEqualToString:@"iPhone"])
            {
                for (int i=0; i<tempToCityNamearr.count; i++)
                {
                    if (f < 95)
                    {
                    f=f+26;
                    }
                }
            }
            else if([deviceType isEqualToString:@"iPhone Simulator"])
            {
                for (int i=0; i<tempToCityNamearr.count; i++) {
                    f=f+26;
                }
            }
            else
            {
                for (int i=0; i<tempToCityNamearr.count; i++)
                {
                    if (f < 135)
                    {
                    f=f+30;
                    }
                }
            }
            if (cityNameArray)
            {
            dropDown = [[NIDropDown alloc]showDropDown:sender :&f :tempToCityNamearr :tempToCityNamearr :@"down"];
            dropDown.tag=2;
            dropDown.delegate = self;
           }
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        [self pickerDismiss];
        
    }
    else
    {
        [dropDown closeDropDown];
        [self rel];
    }
}

#pragma mark -- Days Dropdown
-(IBAction)dropdownOFDaysBtnPressed:(id)sender
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if(dropDown == nil) {
        CGFloat f;
        if([deviceType isEqualToString:@"iPhone"])
        {
            f = 120;
        }
        else if([deviceType isEqualToString:@"iPhone Simulator"])
        {
            f = 120;
        }
        else
        {
            f = 165;
        }
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :rsArr :rsArr :@"down"];
        dropDown.tag=6;
        dropDown.delegate = self;
       
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    [self pickerDismiss];
    
}

#pragma mark -- Type of Car dropdown
-(IBAction)dropdownOFTypeOfCarBtnPressed:(id)sender
{
    [self getcarrecordarray];
    NSString *deviceType = [UIDevice currentDevice].model;
    if(dropDown == nil) {
        CGFloat f=1;
        if([deviceType isEqualToString:@"iPhone"])
        {
            for (int i=0; i<typecarArray.count; i++) {
                if (f < 95)
                {
                    f=f+26;
                }
                
            }
        }
        else if([deviceType isEqualToString:@"iPhone Simulator"])
        {
            for (int i=0; i<typecarArray.count; i++) {
                f=f+26;
            }
        }
        else
        {
            for (int i=0; i<typecarArray.count; i++) {
                if (f < 115)
                {
                f=f+30;
                }
            }
        }
        
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :typecarArray :typecarArray :@"down"];
        dropDown.tag=5;
      //  [dropDown opendropdownupper];

        dropDown.delegate = self;
        dropDownTypeOfCarButton.selected = YES;
        
    }
    else {
        
        [dropDown hideDropDown:sender];
        [self rel];
    }
    [self pickerDismiss];
   
}

-(void)rel{
    //[dropDown release];
    dropDown = nil;
    // dropOnOff = NO;
}

- (void)niDropDownDelegateMethodIndex:(int) index
{
    if (dropDown.tag==1)
    {
        txtfromcity.text=[tempFromCityNamearr objectAtIndex:index];
    }
    if (dropDown.tag==2)
    {
        txttocity.text=[tempToCityNamearr objectAtIndex:index];
    }
    if(dropDown.tag==5)
    {
        txtcar.text =[typecarArray objectAtIndex:index];
    }
    if(dropDown.tag==6)
    {
        txttotalday.text =[rsArr objectAtIndex:index];
    }
    
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

-(void)pickerDismiss
{
    [txtpickupdate resignFirstResponder];
    [txtpickuptime resignFirstResponder];
    [txtname resignFirstResponder];
    [txtemail resignFirstResponder];
    
}

#pragma mark - Menu Show
- (IBAction)show:(id)sender
{
         [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
        [self.sidebarVC panDetected:recoginzer];
}

#pragma mark - Route Selection (one way or round)
-(IBAction)roundtripcheckSelected:(id)sender
{
    _onewayradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    _roundradioimg.image = [UIImage imageNamed:@"redio-check.png"];
   
    onewayagreecheckButton.selected = NO;
    roundtripcheckButton.selected=YES;
    txttotalday.hidden=NO;
    dropdowndaysradioimg.hidden=NO;
    dropDownOfDaysButton.hidden=NO;
    
    _txtReturnDate.hidden =NO;
    returnDateButton.hidden =NO;
    _txtReturnDate.frame = CGRectMake(_txtReturnDate.frame.origin.x, datepickButton.frame.origin.y + 48, _txtReturnDate.frame.size.width, _txtReturnDate.frame.size.height);
    returnDateButton.frame = CGRectMake(_txtReturnDate.frame.origin.x, datepickButton.frame.origin.y + 48, _txtReturnDate.frame.size.width, _txtReturnDate.frame.size.height);
    txtpickuptime.frame = CGRectMake(txtpickuptime.frame.origin.x, _txtReturnDate.frame.origin.y + 48, txtpickuptime.frame.size.width, txtpickuptime.frame.size.height);
    timepickButton.frame = CGRectMake(timepickButton.frame.origin.x, _txtReturnDate.frame.origin.y + 48, timepickButton.frame.size.width, timepickButton.frame.size.height);
    txtcar.frame = CGRectMake(txtcar.frame.origin.x, timepickButton.frame.origin.y + 48, txtcar.frame.size.width, txtcar.frame.size.height);
    dropDownTypeOfCarButton.frame = CGRectMake(txtcar.frame.origin.x, timepickButton.frame.origin.y + 48, txtcar.frame.size.width, txtcar.frame.size.height);
    SubmitbidButton.frame = CGRectMake(SubmitbidButton.frame.origin.x, dropDownTypeOfCarButton.frame.origin.y + 48, SubmitbidButton.frame.size.width, SubmitbidButton.frame.size.height);
  
     daystr = @"Round";
}

-(IBAction)onewaycheckboxSelected:(id)sender
{
    _onewayradioimg.image = [UIImage imageNamed:@"redio-check.png"];
    _roundradioimg.image = [UIImage imageNamed:@"Radio-ucheck.png"];
    onewayagreecheckButton.selected = YES;
    roundtripcheckButton.selected=NO;
    txttotalday.hidden=YES;
    dropDownOfDaysButton.hidden=YES;
    dropdowndaysradioimg.hidden=YES;
    txttotalday.text=@"";
    [self rel];
    [dropDown closeDropDown];
    _txtReturnDate.hidden =YES;
    returnDateButton.hidden =YES;
    txtpickuptime.frame = CGRectMake(txtpickuptime.frame.origin.x, txtpickupdate.frame.origin.y + 48, txtpickuptime.frame.size.width, txtpickuptime.frame.size.height);
    timepickButton.frame = CGRectMake(timepickButton.frame.origin.x, txtpickupdate.frame.origin.y + 48, timepickButton.frame.size.width, timepickButton.frame.size.height);
    txtcar.frame = CGRectMake(txtcar.frame.origin.x, timepickButton.frame.origin.y + 48, txtcar.frame.size.width, txtcar.frame.size.height);
    dropDownTypeOfCarButton.frame = CGRectMake(txtcar.frame.origin.x, timepickButton.frame.origin.y + 48, txtcar.frame.size.width, txtcar.frame.size.height);
    SubmitbidButton.frame = CGRectMake(SubmitbidButton.frame.origin.x, dropDownTypeOfCarButton.frame.origin.y + 48, SubmitbidButton.frame.size.width, SubmitbidButton.frame.size.height);
     daystr = @"OneWay";
}

#pragma mark - Submit Button
-(IBAction)submiteBtnPressed:(id)sender
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
{
    UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alrt show];
}
else
{
    for(int i=0;i<autoCompleteArray2.count;i++)
    {
        if ([[autoCompleteArray2 objectAtIndex:i] isEqualToString:txtfromcity.text])
        {
            fromcityIdStr =[NSString stringWithFormat:@"%@",[cityIdArray2 objectAtIndex:i]];
        }
        
    }
    
    
    for(int i=0;i<autoCompleteArray.count;i++)
    {
        if ([[autoCompleteArray objectAtIndex:i] isEqualToString:txttocity.text])
        {
            tocityIdStr = [NSString stringWithFormat:@"%@",[cityIdArray objectAtIndex:i]];
        }
    }
    
    NSString *fromcitystring = txtfromcity.text;
    NSString *tocitystring = txttocity.text;
    NSString *returnDateString = _txtReturnDate.text;
    NSString *fromcityareastring = txtfromcityarea.text;
   // NSString *tocityareastring = txttocityarea.text;
    NSString *datestring = txtpickupdate.text;
    NSString *timestring = txtpickuptime.text;
    NSString *namestring = txtname.text;
    NSString *emailstring = txtemail.text;
    NSString *carstring = txtcar.text;
    
NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
NSString *fromcityvl = [fromcitystring stringByTrimmingCharactersInSet:whitespace];
NSString *fromcityareavl = [fromcityareastring stringByTrimmingCharactersInSet:whitespace];
NSString *tocityvl = [tocitystring stringByTrimmingCharactersInSet:whitespace];
//NSString *tocityareavl = [tocityareastring stringByTrimmingCharactersInSet:whitespace];
NSString *namevl = [namestring stringByTrimmingCharactersInSet:whitespace];
//NSString *emailvl = [emailstring stringByTrimmingCharactersInSet:whitespace];
NSString *carvl = [carstring stringByTrimmingCharactersInSet:whitespace];
NSString *datevl = [datestring stringByTrimmingCharactersInSet:whitespace];
NSString *timevl = [timestring stringByTrimmingCharactersInSet:whitespace];
    for (int i=0;i<typecaridArray.count; i++) {
        if ([[typecarArray objectAtIndex:i] isEqualToString:carvl]) {
            saveCarId = [NSString stringWithFormat:@"%@",[typecaridArray objectAtIndex:i]];
        }
    }
   // NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
   // NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailReg];
    
    
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        [self alertStatus:@"Check Internet Connection" :@"Warning"];
         [hud hide:YES];
    }
    else if ([fromcityvl isEqualToString:@""] ||  [tocityvl isEqualToString:@""] || [namevl isEqualToString:@""] ||  [timevl isEqualToString:@""] || [datevl isEqualToString:@""] || [carvl isEqualToString:@""] )
    {
        [self alertStatus:@"Please Fill The Fields." :@""];
        [hud hide:YES];
    }
   
//    else if([selecttimeBid isEqualToString:@""])
//    {
//        [self alertStatus:@"Please select the time for bid." :@"Warning!"];
//    }
    else
    {
        if ([daystr isEqualToString:@"OneWay"]) {
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Process";
            [hud show:YES];
            [NSTimer scheduledTimerWithTimeInterval: 0.8 target: self
                                           selector: @selector(checkBid) userInfo: nil repeats: NO];
        }
        else
        {
            if ([returnDateString isEqualToString:@""]) {
                [self alertStatus:@"Please Select Return Date." :@""];
                [hud hide:YES];

            }
            else
            {
                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeIndeterminate;
                hud.labelText = @"Process";
                [hud show:YES];
                [NSTimer scheduledTimerWithTimeInterval: 0.8 target: self
                                               selector: @selector(checkBid) userInfo: nil repeats: NO];
            }
        }
        
        
    }
}
}

#pragma mark -- Check Bid is there or not
-(void)checkBid
{
      NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/check_old_bid.php",appDel.urlStr];
 //   NSString *urlString = [NSString alloc]initWithFormat:@"%@",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\"}",appDel.useridStr];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    NSString *successtr = [jsonDictionary valueForKey:@"Success"];
    NSInteger success = [successtr integerValue];
    
     if(success == 1)
    {
        NSArray *getmsgarr=[[jsonDictionary valueForKey:@"posts"]valueForKey:@"Message"];
        NSString *str = [NSString stringWithFormat:@"%@",[getmsgarr objectAtIndex:0]];
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@""
                                                        message:str
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        
        [alert1 show];

    }
    else if(success == 0)
    {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Process";
        [hud show:YES];
        [NSTimer scheduledTimerWithTimeInterval: 0.8 target: self
                                       selector: @selector(SaveData) userInfo: nil repeats: NO];
       
    }
    [hud hide:YES];

}

- (void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex:- %ld",(long)buttonIndex);
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            
            break;
        case 1: //"Yes" pressed
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Process";
            [hud show:YES];
            [NSTimer scheduledTimerWithTimeInterval: 0.8 target: self
                                           selector: @selector(SaveData) userInfo: nil repeats: NO];
            break;
    }
    
}
#pragma mark -- Save Tour Data
-(void)SaveData
{
  
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/book.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
  
    if ([daystr isEqualToString:@"OneWay"])
    {
        daystr =@"one way";
    }
    else
    {
        NSTimeInterval secondsBetween = [endDate timeIntervalSinceDate:startDate];
        
        int numberOfDays = secondsBetween / 86400;
        
        NSLog(@"There are %d days in between the two dates.", numberOfDays);
        daystr = [NSString stringWithFormat:@"%d",numberOfDays];
        if([daystr isEqualToString:@"0"])
        {
                daystr = @"1";
        }
    }
    

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
   // NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\",\"rout\":\"%@\",\"f_city\":\"%@\",\"f_city_area\":\"%@\",\"t_city\":\"%@\",\"t_city_area\":\"%@\",\"book_date\":\"%@\",\"book_time\":\"%@\",\"name\":\"%@\",\"email\":\"%@\",\"car\":\"%@\"}",appDel.useridStr,txttotalday.text,fromcityIdStr,txtfromcityarea.text,tocityIdStr,txttocityarea.text,txtpickupdate.text,txtpickuptime.text,txtname.text,txtemail.text,saveCarId];//fromcityIdStr
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\",\"rout\":\"%@\",\"f_city\":\"%@\",\"t_city\":\"%@\",\"book_date\":\"%@\",\"book_time\":\"%@\",\"car\":\"%@\"}",appDel.useridStr,daystr,txtfromcity.text,txttocity.text,txtpickupdate.text,txtpickuptime.text,saveCarId];//fromcityIdStr
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    NSString *successtr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"Success"] ;
    NSInteger success = [successtr integerValue];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
       // [self alertStatus:@"Success!" :@""];
        //[self performSegueWithIdentifier:@"ListOfDriverBidViewController" sender:self];
  
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ListOfDriverBidViewController *about = (ListOfDriverBidViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListOfDriverBidViewController"];
        [self.navigationController pushViewController:about animated:YES];
        
    }
    else if(success == 0)
    {
        [self alertStatus:@"Failed!" :@"Failer"];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [hud hide:YES];
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}


#pragma mark - Other
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   
        [dropDown closeDropDown];
        [self rel];
        [self pickerDismiss];
    [[self view] endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[Txt_Field_Mobile_No resignFirstResponder];
     if(textField == txtfromcity)
     {
    [textField resignFirstResponder];
    [txtfromcityarea becomeFirstResponder];
    }
    if(textField == txtfromcityarea)
    {
        [textField resignFirstResponder];
        [txttocity becomeFirstResponder];
    }
    if(textField == txttocity)
    {
        [textField resignFirstResponder];
        [txttocityarea becomeFirstResponder];
    }
    if(textField == txttocityarea)
    {
        [textField resignFirstResponder];
        [txtpickupdate becomeFirstResponder];
    }
    if(textField == txtpickupdate)
    {
        [textField resignFirstResponder];
        [txtpickuptime becomeFirstResponder];
    }
    if(textField == txtpickuptime)
    {
        [textField resignFirstResponder];
        [txtname becomeFirstResponder];
    }
    if(textField == txtname)
    {
        [textField resignFirstResponder];
        [txtemail becomeFirstResponder];
    }
    if(textField == txtemail)
    {
        [textField resignFirstResponder];
        //[txtcar becomeFirstResponder];
        [self dropdownOFTypeOfCarBtnPressed:txtcar];
    }
    
   
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}
- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    
    CGRect frame = self.view.frame;
    CGFloat keyboardHeight = 200.f;
    
    if (up)
    {
        CGRect textFieldFrame = textField.frame;
        CGFloat bottomYPos = textFieldFrame.origin.y + textFieldFrame.size.height;
        
        animateDistance = bottomYPos + 150 + keyboardHeight - frame.size.height;
        if (animateDistance < 0)
            animateDistance = 0;
        else
            animateDistance = fabs(animateDistance);
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    
    if (!(!up && frame.origin.y == 20.f)) {
        if (self.interfaceOrientation == UIDeviceOrientationPortrait)
            frame.origin.y = frame.origin.y + (up ? -animateDistance : animateDistance);
        else if (self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown)
            frame.origin.y = frame.origin.y + (up ? animateDistance : -animateDistance);
        self.view.frame = frame;
    }
    
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

