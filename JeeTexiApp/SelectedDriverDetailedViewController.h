//
//  SelectedDriverDetailedViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface SelectedDriverDetailedViewController : UIViewController
{
    AppDelegate *appDel;
    IBOutlet UIButton *checkagreeButton;
    IBOutlet UIScrollView *Scroll;
    
}

@property(strong,nonatomic) IBOutlet UILabel *lbldrivername;
@property(strong,nonatomic) IBOutlet UILabel *lblprice;
@property(strong,nonatomic) IBOutlet UILabel *lblrating;
@property(strong,nonatomic) IBOutlet UILabel *lblmobileno;
@property(strong,nonatomic) IBOutlet UIImageView *ratingimage;
@property(strong,nonatomic) IBOutlet UILabel *lbltypetrip;
@property(strong,nonatomic) IBOutlet UILabel *lbltypecar;
@property(strong,nonatomic) IBOutlet UILabel *lblcarn;
@property(strong,nonatomic) IBOutlet UILabel *lblpickupdate;
@property(strong,nonatomic) IBOutlet UILabel *lblpickuptime;
@property(strong,nonatomic) IBOutlet UILabel *lblcarno;
@property(strong,nonatomic) IBOutlet UILabel *lbltotal;
@property(strong,nonatomic) IBOutlet UILabel *lblKm;
@property(strong,nonatomic) IBOutlet UITextView *tvfromcity;
@property(strong,nonatomic) IBOutlet UITextView *tvtocity;
@property(strong,nonatomic) IBOutlet UILabel *lblfromcity;
@property(strong,nonatomic) IBOutlet UILabel *lbltocity;


//Line Label set
@property(strong,nonatomic) IBOutlet UILabel *lbllinePickUpUpper;
@property(strong,nonatomic) IBOutlet UILabel *lbllinePickOnUpper;
@property(strong,nonatomic) IBOutlet UILabel *lbllineDropOffUpper;
@property(strong,nonatomic) IBOutlet UILabel *lbllineTotalUpper;
@property(strong,nonatomic) IBOutlet UILabel *lbllineagrrementUpper;
//@property(strong,nonatomic) IBOutlet UILabel *lbltocity;

//Title set
@property(strong,nonatomic) IBOutlet UILabel *lblTitlePickUp;
@property(strong,nonatomic) IBOutlet UILabel *lblTitlePickOn;
@property(strong,nonatomic) IBOutlet UILabel *lblTitleDropOff;
@property(strong,nonatomic) IBOutlet UILabel *lblTitleTotal;
@property(strong,nonatomic) IBOutlet UIImageView *IMGTitleRupee;
@property(strong,nonatomic) IBOutlet UILabel *lblTitleAgree;


@property(strong,nonatomic) IBOutlet UIImageView *carimage;
//@property(strong,nonatomic) IBOutlet UILabel *lblcarnoplace;

@property(strong,nonatomic)NSString *drivernameStr;
@property(strong,nonatomic)NSString *driveridStr;
@property(strong,nonatomic)NSString *drivermobileStr;
@property(strong,nonatomic)NSString *driverpriceStr;
@property(strong,nonatomic)NSString *driverratingStr;
@property(strong,nonatomic)NSString *drivercarnoStr;

-(IBAction)checkagreeBtnClicked:(id)sender;
-(IBAction)paynowBtnClicked:(id)sender;
-(IBAction)paylaterDriverBtnClicked:(id)sender;

-(IBAction)callBtnClicked:(id)sender;
-(IBAction)selectDriverBtnClicked:(id)sender;

@end
