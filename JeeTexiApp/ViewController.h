//
//  ViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 01/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface ViewController : UIViewController 
{
    AppDelegate *appDel;
    IBOutlet UIView *verifyView;
    CGFloat animateDistance;
    IBOutlet UIButton *verificationButton;
    IBOutlet UIButton *resendButton;
}

@property (strong,nonatomic) IBOutlet UITextField *txtmobileno;
@property (strong,nonatomic) IBOutlet UITextField *txtverificationmobileno;

@property (strong,nonatomic) IBOutlet UILabel *lblline;
@property(strong,nonatomic) IBOutlet UILabel *lblverify;


-(IBAction)SendMobileno:(id)sender;
-(IBAction)SendVerificationMobileno:(id)sender;
-(IBAction)ResendVerificationMobileno:(id)sender;

@end

