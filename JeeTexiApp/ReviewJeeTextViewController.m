//
//  ReviewJeeTextViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 04/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "ReviewJeeTextViewController.h"
#import "ManagedBookingViewController.h"
#import "MainPageViewController.h"
#import "Reachability.h"
@interface ReviewJeeTextViewController ()
{
    NSString *ratevalue;
}
@end

@implementation ReviewJeeTextViewController

@synthesize btn_star,lbldrivername,driveridStr,lbl_date,lbl_from,lbl_name,lbl_to,lbl_vahicle;
@synthesize dateStr,timeStr,vahicleStr,namedriverStr,fromcityStr,tocityStr,seatsStr,routeStr;


- (void)viewDidLoad {
    [super viewDidLoad];
    app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString *drivernamestr= [NSString stringWithFormat:@"Mayur Patel"];
    lbldrivername.text = drivernamestr;
    
    BGView.layer.cornerRadius = 7;

    [self Btn_Star_Clicked:Btn_Star_1];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadData) userInfo: nil repeats: NO];
    [hud show:YES];

}


-(void)loadData
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        lbl_name.text = namedriverStr ;
        lbl_date.text = dateStr;
        NSString *vahicalstr = [NSString stringWithFormat:@"%@ (%@ PAX)",vahicleStr,seatsStr];
        lbl_vahicle.text = vahicalstr;
        lbl_from.text = fromcityStr;
        lbl_to.text = tocityStr;
    }
    [hud hide:YES];
    
}

- (IBAction)Btn_Close_Clicked:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    ManagedBookingViewController *SelectDrive = (ManagedBookingViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ManagedBookingViewController"];
    [self.navigationController pushViewController:SelectDrive animated:YES];
 //   NSLog(@"___ %@",app.Search_Data_Array);
}

- (IBAction)Btn_Star_Clicked:(id)sender
{
    if (sender == Btn_Star_1)
    {
        ratevalue = @"1";
        [Btn_Star_1 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_2 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
        [Btn_Star_3 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
        [Btn_Star_4 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
        [Btn_Star_5 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
    }
    else if (sender == Btn_Star_2)
    {
        ratevalue = @"2";
        [Btn_Star_1 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_2 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_3 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
        [Btn_Star_4 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
        [Btn_Star_5 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
    }
    else if (sender == Btn_Star_3)
    {
        ratevalue = @"3";
        [Btn_Star_1 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_2 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_3 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_4 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
        [Btn_Star_5 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
    }
    else if (sender == Btn_Star_4)
    {
        ratevalue = @"4";
        [Btn_Star_1 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_2 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_3 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_4 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_5 setBackgroundImage:[UIImage imageNamed:@"Blank_Star.png"] forState:UIControlStateNormal];
    }
    else if (sender == Btn_Star_5)
    {
        ratevalue = @"5";
        [Btn_Star_1 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_2 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_3 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_4 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
        [Btn_Star_5 setBackgroundImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)Btn_Ok:(id)sender
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(Mobile_No_Registration) userInfo: nil repeats: NO];
    [hud show:YES];
    
}
-(void)Mobile_No_Registration
{

    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/rating.php",app.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    

    NSString *myRequestString =[NSString stringWithFormat:@"{\"d_id\":\"%@\",\"rating\":\"%@\",\"r_id\":\"%@\"}",driveridStr,ratevalue,routeStr];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    
    NSString *successtr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"Success"] ;
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
        [self alertStatus:@"Thank you for giving rating" :@""];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        MainPageViewController *SelectDrive = (MainPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
         [self.navigationController pushViewController:SelectDrive animated:YES];
        // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Rating Failed!" :@"Failer"];
    }
  
    [hud hide:YES];
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/* UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
 
 MainPageViewController *SelectDrive = (MainPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
 [self.navigationController pushViewController:SelectDrive animated:YES];*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
/*
 
 lbl_drivername.text = [NSString stringWithFormat:@"%@",drivernamestring];
 //%@",drivernamestring];
 lbl_drivermobileno.text = [NSString stringWithFormat:@"%@",driverphonenostring];
 //%@",driverphonenostring];
 lbl_vehicle.text = [NSString stringWithFormat:@"%@ (%@ PAX)",[vahiclearr objectAtIndex:0],[seatsarr objectAtIndex:0]];
 lbl_journeydate.text = [NSString stringWithFormat:@"%@",book_datestring];
 lbl_journeytime.text = [NSString stringWithFormat:@"%@",book_timestring];
 //lbl_fromcity.text = [NSString stringWithFormat:@"%@",fromcitystring];
 //lbl_tocity.text = [NSString stringWithFormat:@"%@",tocitystring];
 lbl_pickupLocationname.text = [NSString stringWithFormat:@"%@",fromcityareastring];
 lbl_bookid.text = [NSString stringWithFormat:@"%@",routestring];
 
 lbl_totalfare.text = [NSString stringWithFormat:@"500"];
 
 lbl_pickupLocationname.numberOfLines = 0;
 lbl_pickupLocationname.lineBreakMode = NSLineBreakByWordWrapping;
 CGSize maximumLabelSize = CGSizeMake(lbl_pickupLocationname.frame.size.width, CGFLOAT_MAX);
 CGSize expectSize = [lbl_pickupLocationname sizeThatFits:maximumLabelSize];
 lbl_pickupLocationname.frame = CGRectMake(lbl_pickupLocationname.frame.origin.x, lbl_pickupLocationname.frame.origin.y, expectSize.width, expectSize.height);
 
 
 _lbl_pickuptitletotalfare.frame = CGRectMake(_lbl_pickuptitletotalfare.frame.origin.x, lbl_pickupLocationname.frame.origin.y+expectSize.height+10, _lbl_pickuptitletotalfare.frame.size.width, _lbl_pickuptitletotalfare.frame.size.height);
 
 lbl_totalfare.frame = CGRectMake(lbl_totalfare.frame.origin.x,lbl_pickupLocationname.frame.origin.y+expectSize.height+10, lbl_totalfare.frame.size.width, lbl_journeydate.frame.size.height);
 */

//NSArray *congratesarr = [prefssave arrayForKey:@"congKey"];

//  NSString *drivernamestr = [NSString stringWithFormat:@"Mayur libasiya Patel"];

// NSString *driverphonestr = [NSString stringWithFormat:@"8153856560"];
//  lblphoneno.text = driverphonenostring;
//NSString *bookidstr = [NSString stringWithFormat:@"1235623512"];
// lblbookingid.text = routestring;
//NSString *datestr = [NSString stringWithFormat:@"12/12/2015"];
@end
