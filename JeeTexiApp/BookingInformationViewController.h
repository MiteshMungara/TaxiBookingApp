//
//  BookingInformationViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 11/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface BookingInformationViewController : UIViewController
{
    MBProgressHUD *hud;
    IBOutlet UIScrollView *Scroll;
    AppDelegate *app;
}

@property(strong,nonatomic)IBOutlet UITextView *tv_pickupLocationname;
@property(strong,nonatomic)IBOutlet UILabel *lbl_journeydate;
@property(strong,nonatomic)IBOutlet UILabel *lbl_journeytime;
@property(strong,nonatomic)IBOutlet UILabel *lbl_vehicle;
@property(strong,nonatomic)IBOutlet UILabel *lbl_carno;
@property(strong,nonatomic)IBOutlet UILabel *lbl_totalfare;
@property(strong,nonatomic)IBOutlet UILabel *lbl_drivername;
@property(strong,nonatomic)IBOutlet UILabel *lbl_drivermobileno;
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickupLocationname;
@property(strong,nonatomic)IBOutlet UILabel *lbl_fromcity;
@property(strong,nonatomic)IBOutlet UILabel *lbl_tocity;
@property(strong,nonatomic)IBOutlet UILabel *lbl_bookid;
//Title
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickuptitledate;
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickuptitletime;
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickuptitlevehical;
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickuptitletotalfare;
//@property(strong,nonatomic)IBOutlet UILabel *lbl_drivername;
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickuptitledrivername;
@property(strong,nonatomic)IBOutlet UILabel *lbl_pickuptitledrivermobileno;

-(IBAction)OkBtnPressed:(id)sender;

-(IBAction)backBtnPressed:(id)sender;

@end
