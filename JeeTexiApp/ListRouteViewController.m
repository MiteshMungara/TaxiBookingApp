//
//  ListRouteViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "ListRouteViewController.h"
#import "ListOfDriverBidViewController.h"
#import "SelectedDriverDetailedViewController.h"
#import "SidebarViewController.h"
#import "Reachability.h"
#import "AsyncImageView.h"
@interface ListRouteViewController ()
{
    NSString *drivername;
    NSString *routeidstring;
    NSMutableArray *RouteJsonDictionary;
    NSMutableArray *AllcarTypes;
    NSMutableArray *cellheightarr;
    NSInteger *cellheight;
    NSMutableArray *totalrecordarr;
    
}
@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation ListRouteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    /*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [panGesture delaysTouchesBegan];
    [self.view addGestureRecognizer:panGesture];*/
    self.sidebarVC = [[SidebarViewController alloc] init];
    [self.sidebarVC setBgRGB:0x000000];
    self.sidebarVC.view.frame  = self.view.bounds;
    [self.view addSubview:self.sidebarVC.view];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadData) userInfo: nil repeats: NO];
     [hud show:YES];

}

#pragma mark - Load Data
-(void)loadData
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        datearr  = [[NSMutableArray alloc]init];
        timearr  = [[NSMutableArray alloc]init];
        carimagearr  = [[NSMutableArray alloc]init];
       // fromcityareaarr = [[NSMutableArray alloc]init];
      //  tocityareaarr = [[NSMutableArray alloc]init];
        totalKMarr = [[NSMutableArray alloc]init];
        typeTriparr  = [[NSMutableArray alloc]init];
        //daytriparr  = [[NSMutableArray alloc]init];
        typeofCararr  = [[NSMutableArray alloc]init];
       routeidarr  = [[NSMutableArray alloc]init];
        statusarr  = [[NSMutableArray alloc]init];
        MBProgressHUD *hud;
        AllcarTypes = [[NSMutableArray alloc]init];
        RouteJsonDictionary = [[NSMutableArray alloc]init];
        totalKMarr = [[NSMutableArray alloc]init];
        typeTriparr = [[NSMutableArray alloc]init];
        daytriparr = [[NSMutableArray alloc]init];
        fromcityarr = [[NSMutableArray alloc]init];
        tocityarr = [[NSMutableArray alloc]init];
         totalrecordarr = [[NSMutableArray alloc]init];
      //  nameAddressArr = [[NSMutableArray alloc]init];
        
        // Do any additional setup after loading the view.
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        //app=[[UIApplication sharedApplication]delegate];
        NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/list_of_route.php",app.urlStr];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\"}",app.useridStr];
 
        
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
       NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
        
        NSLog(@"whaole__%@",jsonDictionary);
        NSString *successStr = [jsonDictionary valueForKey:@"Success"];
        NSInteger success = [successStr intValue];
        
        if (success == 1)
        {
            RouteJsonDictionary=[[jsonDictionary valueForKey:@"posts"] mutableCopy];
            routeidarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"id"];
            NSArray *fromcitynamearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city"];
            //NSArray *fromcityareanamearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city_area"];
            
            NSArray *cartypsarray= [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_name"];
            NSArray *carseatsarray= [[jsonDictionary valueForKey:@"posts"]valueForKey:@"seat"];
            NSArray *tocitynamearr  =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"t_city"];
            //NSArray *tocityareanamearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"t_city_area"];
            NSArray *datelocalarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_date"];
            NSArray *timelocalarr=  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
            NSArray *statuslocalarr= [[jsonDictionary valueForKey:@"posts"]valueForKey:@"status"];
            
            
            for (int i=0; i<tocitynamearr.count; i++) {
                NSString *statusstring = [NSString stringWithFormat:@"%@",[statuslocalarr objectAtIndex:i]];
                
                    NSString *tostr = [NSString stringWithFormat:@"%@",[tocitynamearr objectAtIndex:i]];
                    [tocityarr addObject:tostr];
                    NSString *typecar = [NSString stringWithFormat:@"%@ (%@PAX)",[cartypsarray objectAtIndex:i],[carseatsarray objectAtIndex:i]];
                    [AllcarTypes addObject:typecar];
                    NSString *fromstr = [NSString stringWithFormat:@"%@",[fromcitynamearr objectAtIndex:i]];
                    [fromcityarr addObject:fromstr];
                    NSString *datestr = [NSString stringWithFormat:@"%@",[datelocalarr objectAtIndex:i]];
                    [datearr addObject:datestr];
                    NSString *timestr = [NSString stringWithFormat:@"%@",[timelocalarr objectAtIndex:i]];
                    [timearr addObject:timestr];
                    NSString *statusstr = [NSString stringWithFormat:@"%@",[statuslocalarr objectAtIndex:i]];
                    [statusarr addObject:statusstr];
            }
            
            NSArray *totaltypeTriparr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"rout"];
            
            for (int i=0; i<totaltypeTriparr.count; i++)
            {
                NSString *statusstring = [NSString stringWithFormat:@"%@",[statuslocalarr objectAtIndex:i]];
               
                    
                    if ([[totaltypeTriparr objectAtIndex:i] isEqualToString:@"one way"])
                    {
                        [typeTriparr addObject:@"Oneway"];
                        [daytriparr addObject:@""];
                       // [totalKMarr addObject:@"500"];
                    }
                    else
                    {
                        [typeTriparr addObject:@"Roundtrip:"];
                        NSString *daystring = [NSString stringWithFormat:@"%@ ",[totaltypeTriparr objectAtIndex:i]];
                        [daytriparr addObject:daystring];
                        //[totalKMarr addObject:@"600"];
                    }
                
            }
            
        }
        cellheightarr = [[NSMutableArray alloc]init];
        for (int i=0; i<fromcityarr.count; i++) {
            [cellheightarr addObject:@""];
        }
        
        [TableListRoute reloadData];
        }
      
    [hud hide:YES];
    
}

#pragma Menu Button
- (IBAction)show:(id)sender
{
    [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
    [self.sidebarVC panDetected:recoginzer];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return fromcityarr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ListRouteCell";
    
    UITableViewCell *cell = [TableListRoute
                             dequeueReusableCellWithIdentifier:simpleTableIdentifier];
   
    
     NSString *statusstring = [NSString stringWithFormat:@"%@",[statusarr objectAtIndex:indexPath.row]];
    
        cell = nil;
        
        if (cell == nil) {
            cell = [TableListRoute dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        }
    
    UILabel *dateText = (UILabel *)[cell viewWithTag:3];
    dateText.text = [datearr objectAtIndex:indexPath.row];
    
    
    UITextField *timeText = (UITextField *)[cell viewWithTag:4];
    timeText.text = [timearr objectAtIndex:indexPath.row];
    

    
    UILabel *fromcityText = (UILabel *)[cell viewWithTag:1];
    fromcityText.text =[fromcityarr objectAtIndex:indexPath.row];
    fromcityText.numberOfLines = 0;
    fromcityText.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(fromcityText.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [fromcityText sizeThatFits:maximumLabelSize];
    fromcityText.frame = CGRectMake(fromcityText.frame.origin.x, fromcityText.frame.origin.y, fromcityText.frame.size.width, expectSize.height);
       UILabel *linedatedownText = (UILabel *)[cell viewWithTag:16];
    linedatedownText.frame = CGRectMake(linedatedownText.frame.origin.x,fromcityText.frame.size.height+fromcityText.frame.origin.y+4, linedatedownText.frame.size.width, linedatedownText.frame.size.height);
   
    NSLog(@"%f",expectSize.height);
    
    NSString *fromheightStr = [NSString stringWithFormat:@"%f",fromcityText.frame.size.height];
    float fromheight = [fromheightStr floatValue];

    
    
    UILabel *tocityText = (UILabel *)[cell viewWithTag:2];
    tocityText.text = [tocityarr objectAtIndex:indexPath.row];
    tocityText.numberOfLines = 0;
    tocityText.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize1 = CGSizeMake(tocityText.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize1 = [tocityText sizeThatFits:maximumLabelSize1];
    tocityText.frame = CGRectMake(tocityText.frame.origin.x, linedatedownText.frame.origin.y+5, tocityText.frame.size.width, expectSize1.height);
    NSLog(@"To ::%f",expectSize1.height);
   

    UILabel *linetodownText = (UILabel *)[cell viewWithTag:17];
    linetodownText.frame = CGRectMake(linetodownText.frame.origin.x,tocityText.frame.size.height+tocityText.frame.origin.y+4, linetodownText.frame.size.width, linetodownText.frame.size.height);
    NSString *toheightStr = [NSString stringWithFormat:@"%.f",tocityText.frame.size.height];
    float toheight = [toheightStr floatValue];
   
    UIImageView *middelfromcitybetweenimage = (UIImageView *)[cell viewWithTag:21];
    
    middelfromcitybetweenimage.frame = CGRectMake(middelfromcitybetweenimage.frame.origin.x, linedatedownText.frame.origin.y-20, middelfromcitybetweenimage.frame.size.width, middelfromcitybetweenimage.frame.size.height);

    
    UILabel *typecarText = (UILabel *)[cell viewWithTag:8];
    typecarText.text = [AllcarTypes objectAtIndex:indexPath.row];
    typecarText.numberOfLines = 0;
    typecarText.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSizetypecar = CGSizeMake(typecarText.frame.size.width, CGFLOAT_MAX);
    CGSize expectSizetypecar = [typecarText sizeThatFits:maximumLabelSizetypecar];
    typecarText.frame = CGRectMake(typecarText.frame.origin.x, linetodownText.frame.origin.y+4, typecarText.frame.size.width, expectSizetypecar.height);
    
    NSString *typecarheightStr = [NSString stringWithFormat:@"%.f",typecarText.frame.size.height];
    float typecarheight = [typecarheightStr floatValue];
   
    
    UILabel *typeTripText = (UILabel *)[cell viewWithTag:5];
    typeTripText.text = [typeTriparr objectAtIndex:indexPath.row];
    typeTripText.frame = CGRectMake(self.view.frame.size.width-110,linetodownText.frame.size.height+linetodownText.frame.origin.y+4, typeTripText.frame.size.width, typeTripText.frame.size.height);
    UILabel *daytripText = (UILabel *)[cell viewWithTag:9];
    daytripText.text = [daytriparr objectAtIndex:indexPath.row];
    daytripText.frame = CGRectMake(typeTripText.frame.origin.x+25,linetodownText.frame.size.height+ linetodownText.frame.origin.y+4, daytripText.frame.size.width, daytripText.frame.size.height);
    
    UILabel *linecarnamedownText = (UILabel *)[cell viewWithTag:18];
    linecarnamedownText.frame = CGRectMake(linecarnamedownText.frame.origin.x,typecarText.frame.size.height+typecarText.frame.origin.y+4, linecarnamedownText.frame.size.width, linecarnamedownText.frame.size.height);

    UILabel *statustitleText = (UILabel *)[cell viewWithTag:22];
    statustitleText.frame = CGRectMake(statustitleText.frame.origin.x,linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, statustitleText.frame.size.width, statustitleText.frame.size.height);
    int addition = roundf(toheight) + roundf(fromheight) + roundf(typecarheight)+ roundf(7.00);
    [cellheightarr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",addition]];
    NSLog(@"total%d",addition);
    if ([statusstring isEqualToString:@"0"]) {
        UILabel *statusText = (UILabel *)[cell viewWithTag:10];
        statusText.text = [NSString stringWithFormat:@"Pending"];
         statusText.frame = CGRectMake(statusText.frame.origin.x,linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, statusText.frame.size.width, statusText.frame.size.height);
        statusText.textColor = [UIColor colorWithRed:248/255. green:195/255. blue:24/255. alpha:1];
        //Buuton
        UIButton *getrecieptButton = (UIButton *)[cell viewWithTag:11];
        getrecieptButton.frame = CGRectMake(getrecieptButton.frame.origin.x,+linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, getrecieptButton.frame.size.width, getrecieptButton.frame.size.height);
    [getrecieptButton addTarget:self action:@selector(getreceiptButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        int addition = roundf(toheight) + roundf(fromheight) + roundf(typecarheight)+ roundf(7.00);
        [cellheightarr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",addition]];
        NSLog(@"total%d",addition);

    }
    else if ([statusstring isEqualToString:@"1"])
    {
        UILabel *statusText = (UILabel *)[cell viewWithTag:10];
        statusText.text = [NSString stringWithFormat:@"Cancelled"];
         statusText.frame = CGRectMake(statusText.frame.origin.x,linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, statusText.frame.size.width, statusText.frame.size.height);
        UIButton *getrecieptButton = (UIButton *)[cell viewWithTag:11];
        getrecieptButton.frame = CGRectMake(getrecieptButton.frame.origin.x,statustitleText.frame.origin.y-statustitleText.frame.size.height-15, getrecieptButton.frame.size.width, getrecieptButton.frame.size.height);
        getrecieptButton.hidden=YES;
        statusText.textColor = [UIColor redColor];
        int addition = roundf(toheight) + roundf(fromheight) + roundf(typecarheight);
         [cellheightarr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",addition]];
        NSLog(@"total%d",addition);

    }
    else if ([statusstring isEqualToString:@"2"])
    {
        UILabel *statusText = (UILabel *)[cell viewWithTag:10];
        statusText.text = [NSString stringWithFormat:@"Booked"];
         statusText.frame = CGRectMake(statusText.frame.origin.x,linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, statusText.frame.size.width, statusText.frame.size.height);
        UIButton *getrecieptButton = (UIButton *)[cell viewWithTag:11];
        getrecieptButton.frame = CGRectMake(getrecieptButton.frame.origin.x,statustitleText.frame.origin.y-statustitleText.frame.size.height-15, getrecieptButton.frame.size.width, getrecieptButton.frame.size.height);
        getrecieptButton.hidden=YES;
        statusText.textColor = [UIColor greenColor];
        int addition = roundf(toheight) + roundf(fromheight) + roundf(typecarheight);
         [cellheightarr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",addition]];
        NSLog(@"total%d",addition);

    }
    else if ([statusstring isEqualToString:@"3"])
    {
        UILabel *statusText = (UILabel *)[cell viewWithTag:10];
        statusText.text = [NSString stringWithFormat:@"Success"];
         statusText.frame = CGRectMake(statusText.frame.origin.x,linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, statusText.frame.size.width, statusText.frame.size.height);
        UIButton *getrecieptButton = (UIButton *)[cell viewWithTag:11];
        getrecieptButton.frame = CGRectMake(getrecieptButton.frame.origin.x,statustitleText.frame.origin.y-statustitleText.frame.size.height-15, getrecieptButton.frame.size.width, getrecieptButton.frame.size.height);
        getrecieptButton.hidden=YES;
        statusText.textColor = [UIColor greenColor];
        int addition = roundf(toheight) + roundf(fromheight) + roundf(typecarheight);
        [cellheightarr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",addition]];
        NSLog(@"total%d",addition);

    }
    else if ([statusstring isEqualToString:@"4"])
    {
        UILabel *statusText = (UILabel *)[cell viewWithTag:10];
        statusText.text = [NSString stringWithFormat:@"Lost"];
        statusText.frame = CGRectMake(statusText.frame.origin.x,linecarnamedownText.frame.size.height+linecarnamedownText.frame.origin.y+4, statusText.frame.size.width, statusText.frame.size.height);
        UIButton *getrecieptButton = (UIButton *)[cell viewWithTag:11];
        getrecieptButton.frame = CGRectMake(getrecieptButton.frame.origin.x,statustitleText.frame.origin.y-statustitleText.frame.size.height-15, getrecieptButton.frame.size.width, getrecieptButton.frame.size.height);
        getrecieptButton.hidden=YES;
        statusText.textColor = [UIColor greenColor];
        int addition = roundf(toheight) + roundf(fromheight) + roundf(typecarheight);
         [cellheightarr replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%d",addition]];
        NSLog(@"total%d",addition);
        
    }
        UIButton *deleteButton = (UIButton *)[cell viewWithTag:24];
         [deleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    TableListRoute.separatorStyle = UITableViewCellSeparatorStyleNone;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    
    
    return cell;
    
}

#pragma mark --> Height of Tableview Set
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger cellheights;
    NSLog(@"hieths cell %ld",(long)indexPath.row);
    NSString *str = [NSString stringWithFormat:@"%@",[cellheightarr objectAtIndex:indexPath.row]];
    NSInteger rowsize = [str intValue];
    if (rowsize > 50) {
        NSString *height = [NSString stringWithFormat:@"%@",[cellheightarr objectAtIndex:indexPath.row]];
        
        cellheights = [height intValue];
        NSLog(@"height cell %ld",cellheights);
        return 80+cellheights;
    }
    else
    {
        return 130;
    }
    
    
    
}

#pragma mark --> TableView DidSelect Method
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     routeidstring = [NSString stringWithFormat:@"%@",[routeidarr objectAtIndex:indexPath.row]];
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     [prefs setObject:routeidstring forKey:@"idKey"];
     NSLog(@"__%@",routeidstring);
     
     [self performSegueWithIdentifier:@"ListOfDriverBidViewController" sender:self];*/
    
}

#pragma mark --> TableView CommitEditingStyle Method
/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [routeidarr removeObjectAtIndex:indexPath.row];
    
    [TableListRoute reloadData];
}*/

#pragma mark - Delete Record
-(void)deleteButtonClicked:sender
{
    [self alertShow];
    UIButton *senderButton = (UIButton *)sender;
        UITableViewCell *buttonCell = (UITableViewCell *)senderButton.superview.superview;
        NSIndexPath* pathOfTheCell = [TableListRoute indexPathForCell:buttonCell];
       NSLog(@"pathOfTheCell.row :%ld",(long)pathOfTheCell.row);
    routeidstring = [NSString stringWithFormat:@"%@",[routeidarr objectAtIndex:pathOfTheCell.row]];
//    
//    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
//    {
//        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alrt show];
//    }
//    else
//    {
//    UIButton *senderButton = (UIButton *)sender;
//    UITableViewCell *buttonCell = (UITableViewCell *)senderButton.superview.superview;
//    NSIndexPath* pathOfTheCell = [TableListRoute indexPathForCell:buttonCell];
//    NSLog(@"pathOfTheCell.row :%ld",(long)pathOfTheCell.row);
//    routeidstring = [NSString stringWithFormat:@"%@",[routeidarr objectAtIndex:pathOfTheCell.row]];
//    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/remove_route.php",app.urlStr];
//    NSURL *url = [NSURL URLWithString:urlString];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    
//    NSString *myRequestString =[NSString stringWithFormat:@"{\"r_id\":\"%@\"}",routeidstring];
//    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
//    
//    [request setHTTPMethod: @"POST"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody: requestData];
//    NSError *error;
//    NSURLResponse *response;
//    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    
//    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
//    NSString *sucessStr = [jsonDictionary valueForKey:@"Success"];
//    NSInteger success = [sucessStr intValue];
//    if (success == 1)
//    {
//        [self loadData];
//    }
//    
//    }
}

#pragma mark - Get Receipt Button
-(void)getreceiptButtonClicked:sender
{
    UIButton *senderButton = (UIButton *)sender;
    UITableViewCell *buttonCell = (UITableViewCell *)senderButton.superview.superview;
    NSIndexPath* pathOfTheCell = [TableListRoute indexPathForCell:buttonCell];
    NSLog(@"pathOfTheCell.row :%ld",(long)pathOfTheCell.row);
    routeidstring = [NSString stringWithFormat:@"%@",[routeidarr objectAtIndex:pathOfTheCell.row]];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:routeidstring forKey:@"idKey"];
    NSMutableArray *RouteJsonArray = [[NSMutableArray alloc]init];
    [RouteJsonArray addObject:[RouteJsonDictionary objectAtIndex:pathOfTheCell.row]];
    [prefs setObject:RouteJsonArray forKey:@"RouteJsonDictionary"];
    NSLog(@"route str__%@",routeidstring);
    NSLog(@"selected__%@",RouteJsonArray);
    [self performSegueWithIdentifier:@"ListOfDriverBidViewController" sender:self];
}

#pragma mark - AlertView
-(void)alertShow
{
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@""
                                                     message:@"Are you sure want to remove this route?"
                                                    delegate:self
                                           cancelButtonTitle:@"No"
                                           otherButtonTitles:@"Yes", nil];

    [alert1 show];
    
}
- (void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex:- %ld",(long)buttonIndex);
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            
            break;
        case 1: //"Yes" pressed
            [self deleterecord];
            
            break;
    }
    
}

#pragma mark - Delete
-(void)deleterecord
{
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/remove_task.php",app.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"route_id\":\"%@\"}",routeidstring];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    NSString *sucessStr = [jsonDictionary valueForKey:@"Success"];
    NSInteger success = [sucessStr intValue];
    if (success == 1)
    {
        [self loadData];
        
    }

}
/*
-(void)CancleButtonClicked:sender
{
    
    UIButton *senderButton = (UIButton *)sender;
    UITableViewCell *buttonCell = (UITableViewCell *)senderButton.superview.superview;
    NSIndexPath* pathOfTheCell = [TableListRoute indexPathForCell:buttonCell];
    NSLog(@"pathOfTheCell.row :%ld",(long)pathOfTheCell.row);
     routeidstring = [NSString stringWithFormat:@"%@",[routeidarr objectAtIndex:pathOfTheCell.row]];
    [self alertShow];
   
}*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ListOfDriverBidViewController"])
    {
        ListOfDriverBidViewController *vc = [segue destinationViewController];
        app.routeidStr = routeidstring;
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//  fromcityarr = [NSArray arrayWithObjects:@"Surat",@"Rajkot",@"Jamnagar",@"Amrelli", nil];
//  tocityarr = [NSArray arrayWithObjects:@"Ahmdabad",@"Bhuj",@"Akleshware",@"Bagasra", nil];
//  fromcityareaarr = [NSArray arrayWithObjects:@"Katargam",@"MovadiChowk",@"Jamsdar",@"Rakka", nil];
//  tocityareaarr = [NSArray arrayWithObjects:@"Kalaper",@"Dajkar",@"Sachin",@"Abevadi Road", nil];
// totalKMarr = [NSArray arrayWithObjects:@"350KM",@"400KM",@"450KM",@"200KM", nil];
//  typeofCararr = [NSArray arrayWithObjects:@"Tufan",@"BMW",@"Marcidice",@"Metadore", nil];

// typeTriparr = [NSArray arrayWithObjects:@"Round Trip",@"One Way",@"Round Trip",@"Round Trip", nil];


//  datearr = [NSArray arrayWithObjects:@"3/3/2015",@"15/6/2015",@"13/5/2015",@"06/11/2015", nil];
//  timearr = [NSArray arrayWithObjects:@"15:05",@"04:44",@"01:18",@"04:25", nil];
// daytriparr = [NSArray arrayWithObjects:@"10 Days",@"",@"18 Days",@"05 Days", nil];
