//
//  AboutUsViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "AboutUsViewController.h"
#import "MainPageViewController.h"
#import "SidebarViewController.h"
@interface AboutUsViewController ()
@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation AboutUsViewController
@synthesize lblnareshdegree,lblyogeshname,lblnareshname,lblyogeshdegree,txtnareshDescription,txtyogeshDescription;
- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
     Scroll.contentSize = CGSizeMake(320, 500);
    /*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [panGesture delaysTouchesBegan];
    [self.view addGestureRecognizer:panGesture];*/
    self.sidebarVC = [[SidebarViewController alloc] init];
    [self.sidebarVC setBgRGB:0x000000];
    self.sidebarVC.view.frame  = self.view.bounds;
    [self.view addSubview:self.sidebarVC.view];
    
    //lblyogeshname.text = @"";
    //lblyogeshdegree.text = @".";
  //  self.yogeshimage.image = [UIImage imageNamed:@"yogen.png"];
  
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadDataAboutus) userInfo: nil repeats: NO];
    [hud show:YES];



}


-(void)loadDataAboutus
{
    
    // txtyogeshDescription.text= @"";
    
    
    //lblnareshname.text = @"";
    // lblnareshdegree.text = @"";
    //self.nareshimage.image = [UIImage imageNamed:@"naren.png"];
    // txtnareshDescription.text= @"";
    
    // self.navigationItem.hidesBackButton = YES;
    //self.yogeshimage.layer.cornerRadius = 10.0f;
   // self.nareshimage.layer.cornerRadius = 10.0f;
    lblyogeshname.text = @"Yogen Singh";
    lblyogeshdegree.text = @"BE Electronics - 1999 Batch Passout.";
    lblyogeshdegree.numberOfLines = 0;
    lblyogeshdegree.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSizeyogeshdesc = CGSizeMake(lblyogeshdegree.frame.size.width, CGFLOAT_MAX);
    CGSize expectSizeyogeshdesc = [lblyogeshdegree sizeThatFits:maximumLabelSizeyogeshdesc];
    lblyogeshdegree.frame = CGRectMake(lblyogeshdegree.frame.origin.x,lblyogeshname.frame.size.height+lblyogeshname.frame.origin.y+8, expectSizeyogeshdesc.width, expectSizeyogeshdesc.height);
    
    lblyogeshdegree.frame = CGRectMake(lblyogeshdegree.frame.origin.x,lblyogeshname.frame.size.height+ lblyogeshname.frame.origin.y+4, lblyogeshname.frame.size.width, lblyogeshdegree.frame.size.height);
    self.yogeshimage.image = [UIImage imageNamed:@"yogen.png"];
    self.yogeshimage.layer.cornerRadius = 10.0f;
    
    /*_lblyogeshDescription.text= @"    He started his career as an employee in HR consultancy .He last worked for an Australian MNC in oil and gas stream. Quitting that Yogen started eco-tourism travel through his own camp in Nanital known as campxplore (www.campxplore.com). Now he is full time with jee Taxi.";
    _lblyogeshDescription.numberOfLines = 0;
    _lblyogeshDescription.lineBreakMode = NSLineBreakByWordWrapping;
    _lblyogeshDescription.textAlignment= NSTextAlignmentJustified;
    CGSize maximumLabelSize = CGSizeMake(_lblyogeshDescription.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [_lblyogeshDescription sizeThatFits:maximumLabelSize];
    _lblyogeshDescription.frame = CGRectMake(_lblyogeshDescription.frame.origin.x,_yogeshimage.frame.size.height+_yogeshimage.frame.origin.y+8, expectSize.width, expectSize.height);*/
    
    
    //===========================  Naresh ===========================================
    
    lblnareshname.text = @"Naren Shekhawat";
    lblnareshname.frame = CGRectMake(lblnareshname.frame.origin.x,_tvyogeshdescription.frame.size.height+_tvyogeshdescription.frame.origin.y+29, lblnareshname.frame.size.width, lblnareshname.frame.size.height);
    lblnareshdegree.text = @"BE Electronics - 1999 Batch Passout.";
    CGSize maximumLabelSizenareshdesc = CGSizeMake(lblnareshdegree.frame.size.width, CGFLOAT_MAX);
    CGSize expectSizenareshdesc = [lblnareshdegree sizeThatFits:maximumLabelSizenareshdesc];
    lblnareshdegree.frame = CGRectMake(lblnareshdegree.frame.origin.x,lblnareshname.frame.size.height+lblnareshname.frame.origin.y+8, expectSizenareshdesc.width, expectSizenareshdesc.height);
    
    self.nareshimage.image = [UIImage imageNamed:@"naren.png"];
    self.nareshimage.frame = CGRectMake(self.nareshimage.frame.origin.x,_tvyogeshdescription.frame.size.height+ _tvyogeshdescription.frame.origin.y+25, self.nareshimage.frame.size.width, self.nareshimage.frame.size.height);
    self.nareshimage.layer.cornerRadius = 10.0f;
    
     self.tvnareshdescription.frame = CGRectMake(self.tvnareshdescription.frame.origin.x,self.nareshimage.frame.size.height+ self.nareshimage.frame.origin.y+8, self.tvnareshdescription.frame.size.width, self.tvnareshdescription.frame.size.height);
    
   // _lblnareshDescription.text= @"    He carried civil works in Bombay in the initial years of his carrier.In his entrepreneurial debut he started a playschool in Bombay named FlyingStation (www.flyingstation.in) . Now he is full time with Jee Taxi.";
    
  /*  _tvnareshdescription.numberOfLines = 0;
    _lblnareshDescription.lineBreakMode = NSLineBreakByWordWrapping;
    _lblnareshDescription.textAlignment= NSTextAlignmentJustified;
    CGSize maximumLabelSizeyogesh = CGSizeMake(_lblyogeshDescription.frame.size.width, CGFLOAT_MAX);
    CGSize expectSizeyogesh = [_lblnareshDescription sizeThatFits:maximumLabelSizeyogesh];
    _lblnareshDescription.frame = CGRectMake(_lblnareshDescription.frame.origin.x,self.nareshimage.frame.size.height+self.nareshimage.frame.origin.y+8, expectSizeyogesh.width, expectSizeyogesh.height);*/
    [hud hide:YES];
}
- (IBAction)show:(id)sender
{
    [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
    [self.sidebarVC panDetected:recoginzer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
