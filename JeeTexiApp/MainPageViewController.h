//
//  MainPageViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface MainPageViewController : UIViewController  <NIDropDownDelegate,UITextFieldDelegate>
{
    AppDelegate *appDel;
    NIDropDown *dropDown;
    IBOutlet UIButton *radioSelectedButton;
    IBOutlet UIButton *roundtripcheckButton;
    IBOutlet UIButton *onewayagreecheckButton;
    UIDatePicker *datePicker;
     UIDatePicker *returndatePicker;
    UIDatePicker *timePicker;
    IBOutlet UIButton *dropDownTypeOfCarButton;
    IBOutlet UIButton *dropDownOfDaysButton;
    IBOutlet UIButton *datepickButton;
    IBOutlet UIButton *returndateButton;
    IBOutlet UIButton *timepickButton;
    IBOutlet UIButton *radiothirtyminButton;
    IBOutlet UIButton *radiofourtyfiveminButton;
    IBOutlet UIButton *radioonehourButton;
    IBOutlet UIButton *returnDateButton;
    IBOutlet UIButton *SubmitbidButton;
    IBOutlet UIScrollView *Scroll;
    CGFloat animateDistance;
    MBProgressHUD *hud;
}

@property(strong,nonatomic)IBOutlet UITextField *txttotalday;
@property(strong,nonatomic)IBOutlet UITextField *txtfromcity;
@property(strong,nonatomic)IBOutlet UITextField *txtfromcityarea;
@property(strong,nonatomic)IBOutlet UITextField *txttocity;
@property(strong,nonatomic)IBOutlet UITextField *txttocityarea;
@property(strong,nonatomic)IBOutlet UITextField *txtpickupdate;
@property(strong,nonatomic)IBOutlet UITextField *txtpickuptime;
@property(strong,nonatomic)IBOutlet UITextField *txtname;
@property(strong,nonatomic)IBOutlet UITextField *txtemail;
@property(strong,nonatomic)IBOutlet UITextField *txtcar;
@property(strong,nonatomic)IBOutlet UITextField *txtReturnDate;


@property (strong,nonatomic) IBOutlet UIImageView *dropdowndaysradioimg;
@property (strong,nonatomic) IBOutlet UIImageView *roundradioimg;
@property (strong,nonatomic) IBOutlet UIImageView *onewayradioimg;

@property (strong,nonatomic) IBOutlet UIImageView *thiredradioimg;
@property (strong,nonatomic) IBOutlet UIImageView *fourthradioimg;
@property (strong,nonatomic) IBOutlet UIImageView *onehourradioimg;

-(IBAction)checkBidthirtyTimeBtnPressed:(id)sender;
-(IBAction)checkBidfourtyfiveTimeBtnPressed:(id)sender;
-(IBAction)checkBidonehourTimeBtnPressed:(id)sender;

-(IBAction)dateBtnClicked:(id)sender;
-(IBAction)timeBtnClicked:(id)sender;

-(IBAction)submiteBtnPressed:(id)sender;

-(IBAction)roundtripcheckSelected:(id)sender;
-(IBAction)onewaycheckboxSelected:(id)sender;
-(IBAction)ReturnDateBtnPressed:(id)sender;
@end
