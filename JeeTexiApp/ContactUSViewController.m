//
//  ContactUSViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "ContactUSViewController.h"
#import "MainPageViewController.h"

#import "SidebarViewController.h"
@interface ContactUSViewController ()
@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation ContactUSViewController
@synthesize txtname,txtemail,txtcity,txtdetail,txtphoneno;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // Scroll.contentSize = CGSizeMake(320, 650);
   
/*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
[panGesture delaysTouchesBegan];
[self.view addGestureRecognizer:panGesture];*/
self.sidebarVC = [[SidebarViewController alloc] init];
[self.sidebarVC setBgRGB:0x000000];
self.sidebarVC.view.frame  = self.view.bounds;
[self.view addSubview:self.sidebarVC.view];

}
- (IBAction)show:(id)sender
{
    [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
    [self.sidebarVC panDetected:recoginzer];
}


-(IBAction)submitContactBtnPressed:(id)sender
{
    
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailReg];
        
    if ([txtname.text isEqualToString:@""] || [txtemail.text isEqualToString:@""] || [txtphoneno.text isEqualToString:@""] || [txtcity.text isEqualToString:@""] || [txtdetail.text isEqualToString:@""])
    {
        [self alertStatus:@"Please Fill The Fields." :@""];
    }
    else if ([emailTest evaluateWithObject:txtemail.text] != YES)
    {
        [self alertStatus:@"Please enter a valid email address." :@"Warning!"];
    }
    else
    {
        NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/contact_us.php",appDel.urlStr];
        NSString *myRequestString =[NSString stringWithFormat:@"{\"name\":\"%@\",\"email_id\":\"%@\",\"phone_no\":\"%@\",\"city\":\"%@\",\"details\":\"%@\"}",txtname.text,txtemail.text,txtphoneno.text,txtcity.text,txtdetail.text];
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
        
        NSString *successtr = [[jsonDictionary valueForKey:@"post"]valueForKey:@"Success"] ;
        NSInteger success = [successtr integerValue];
        
        if(success == 2)
        {
            [self alertStatus:@"User already exist." :@"Warning!"];
        }
        else if(success == 1)
        {
            [self alertStatus:@"Will contact you soon." :@""];
            txtname.text=@"";
            txtphoneno.text=@"";
            txtcity.text=@"";
            txtdetail.text=@"";
            txtemail.text=@"";
            [txtdetail resignFirstResponder];
        }
        else if(success == 0)
        {
            [self alertStatus:@"Failed!" :@"Failer"];
        }
    }
    }

}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}



- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}
- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    
    CGRect frame = self.view.frame;
    CGFloat keyboardHeight = 200.f;
    
    if (up)
    {
        CGRect textFieldFrame = textField.frame;
        CGFloat bottomYPos = textFieldFrame.origin.y + textFieldFrame.size.height;
        
        animateDistance = bottomYPos + 150 + keyboardHeight - frame.size.height;
        if (animateDistance < 0)
            animateDistance = 0;
        else
            animateDistance = fabs(animateDistance);
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    
    if (!(!up && frame.origin.y == 20.f)) {
        if (self.interfaceOrientation == UIDeviceOrientationPortrait)
            frame.origin.y = frame.origin.y + (up ? -animateDistance : animateDistance);
        else if (self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown)
            frame.origin.y = frame.origin.y + (up ? animateDistance : -animateDistance);
        self.view.frame = frame;
    }
    
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
