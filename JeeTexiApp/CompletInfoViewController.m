//
//  CompletInfoViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 10/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "CompletInfoViewController.h"
#import "MVPlaceSearchTextField.h"
NSString *apiKey = @"AIzaSyCfwGrtPmrGnhafCZBj_RiVFs6_cjUysUE";

@interface CompletInfoViewController ()<PlaceSearchTextFieldDelegate,UITextFieldDelegate>

{
    NSString *searchWord;
    NSMutableArray *searchResults;
    
}
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtPlaceSearch;

@end

@implementation CompletInfoViewController
@synthesize txtname,lbltext;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    _txtPlaceSearch.placeSearchDelegate                 = self;
    _txtPlaceSearch.strApiKey                           = @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM";
    _txtPlaceSearch.superViewOfList                     = self.view;  // View, on which Autocompletion list should be appeared.
    _txtPlaceSearch.autoCompleteShouldHideOnSelection   = YES;
    _txtPlaceSearch.maximumNumberOfAutoCompleteRows     = 5;

  //  [txtname addTarget:self action:@selector(AutofillFromCitytext:) forControlEvents:UIControlEventEditingChanged];
    
    

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewDidAppear:(BOOL)animated{
    
    //Optional Properties
    //Optional Properties
    _txtPlaceSearch.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
    _txtPlaceSearch.autoCompleteBoldFontName = @"HelveticaNeue";
    _txtPlaceSearch.autoCompleteTableCornerRadius=0.0;
    _txtPlaceSearch.autoCompleteRowHeight=35;
    _txtPlaceSearch.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    _txtPlaceSearch.autoCompleteFontSize=14;
    _txtPlaceSearch.autoCompleteTableBorderWidth=1.0;
    _txtPlaceSearch.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=YES;
    _txtPlaceSearch.autoCompleteShouldHideOnSelection=YES;
    _txtPlaceSearch.autoCompleteShouldHideClosingKeyboard=YES;
    _txtPlaceSearch.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    _txtPlaceSearch.autoCompleteTableFrame = CGRectMake((self.view.frame.size.width-_txtPlaceSearch.frame.size.width)*0.5, _txtPlaceSearch.frame.size.height, _txtPlaceSearch.frame.size.width, 200.0);
    
    
    
    lbltext.numberOfLines = 0;
    lbltext.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(lbltext.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [lbltext sizeThatFits:maximumLabelSize];
    lbltext.frame = CGRectMake(lbltext.frame.origin.x, lbltext.frame.origin.y, expectSize.width, expectSize.height);

    [txtname sizeToFit];
}

#pragma mark - Place search Textfield Delegates
-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
    [self.view endEditing:YES];
    NSLog(@"%@",responseDict);
    
    NSDictionary *aDictLocation=[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"];
    NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
}
-(void)placeSearchWillShowResult{
    
}
-(void)placeSearchWillHideResult{
    
}
-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
     cell.contentView.backgroundColor = [UIColor whiteColor];
    
}


/*
-(void)AutofillFromCitytext:sender{
    
    searchWord=txtname.text;

    searchWord = searchWord.lowercaseString;
    
    searchResults = [NSMutableArray array];
    
    
    //NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&city=%@",searchWord];
    
    // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=(cities)&key=%s
    // NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&componentRestrictions:{country:india}&types=(cities)&key=%@",searchWord,apiKey];
    
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&language=en&key=%@",searchWord,apiKey];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];;
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                //  completion(NO, newError);
                return;
            }
            //    completion(NO, error);
            return;
        } else {
            NSLog(@"jSONresult%@",jSONresult);
            NSArray *results;
            results = [jSONresult valueForKey:@"predictions"];
            NSLog(@"results %@",results);
            [searchResults addObject:results];
            NSArray *name = [[[searchResults valueForKey:@"terms"]objectAtIndex:0]valueForKey:@"value"];
            /*
             for (NSDictionary *jsonDictionary in results) {
             ABCGoogleAutoCompleteResult *location = [[ABCGoogleAutoCompleteResult alloc] initWithJSONData:jsonDictionary];
             [searchResults addObject:location];
             }*/


//===========
            /*NSLog(@"%@",searchResults );
            
            NSLog(@"%@",searchResults );
            // [self.searchResultsCache setObject:self.searchResults forKey:searchWord];
            
            // completion(YES, nil);
            [self DropDownAutoSearchCity:0];
        }
    }];
    
    [task resume];
    
    

}


-(void)DropDownAutoSearchCity:(id)sender
{
    //[self AutofillFromCitytext];
     NSArray *namearr = [[[searchResults valueForKey:@"terms"]objectAtIndex:0]valueForKey:@"value"];
    NSString *deviceType = [UIDevice currentDevice].model;
    if(dropDown == nil) {
        CGFloat f;
        if([deviceType isEqualToString:@"iPhone"])
        {
            f = 120;
        }
        else if([deviceType isEqualToString:@"iPhone Simulator"])
        {
            f = 120;
        }
        else
        {
            f = 165;
        }
        app.opendropdownupper =@"dropdown";
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :namearr :namearr :@"down"];
        dropDown.tag=5;
        //  [dropDown opendropdownupper];
        
        dropDown.delegate = self;
        //dropDownTypeOfCarButton.selected = YES;
        
    }
    else {
        
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}

-(void)rel{
    //[dropDown release];
    dropDown = nil;
    // dropOnOff = NO;
}

- (void)niDropDownDelegateMethodIndex:(int) index
{
    NSArray *namearr = [[[searchResults valueForKey:@"terms"]objectAtIndex:0]valueForKey:@"value"];
    
    if(dropDown.tag==5)
    {
        txtname.text =[namearr objectAtIndex:index];
    }
    
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

-(void)pickerDismiss
{
    
    [txtname resignFirstResponder];
}
*/
/*
-(NSString *)name{
    NSString *name = [NSString new];
    if([_jsonDictionary[@"terms"] objectAtIndex:0][@"value"] != [NSNull null]){
        name = [_jsonDictionary[@"terms"] objectAtIndex:0][@"value"];
    }
    return name;
}*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Properties


-(IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
