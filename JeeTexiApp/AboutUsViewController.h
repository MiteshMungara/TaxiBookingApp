//
//  AboutUsViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface AboutUsViewController : UIViewController
{
    AppDelegate *appDel;
    IBOutlet UIScrollView *Scroll;
    MBProgressHUD *hud;
}

@property(strong,nonatomic) IBOutlet UITextView *txtyogeshDescription;
@property(strong,nonatomic) IBOutlet UILabel *lblyogeshname;
@property(strong,nonatomic) IBOutlet UILabel *lblyogeshdegree;
@property(strong,nonatomic) IBOutlet UIImageView *yogeshimage;
@property(strong,nonatomic) IBOutlet UILabel *lblyogeshDescription;

@property(strong,nonatomic) IBOutlet UITextView *txtnareshDescription;
@property(strong,nonatomic) IBOutlet UILabel *lblnareshname;
@property(strong,nonatomic) IBOutlet UILabel *lblnareshdegree;
@property(strong,nonatomic) IBOutlet UIImageView *nareshimage;
@property(strong,nonatomic) IBOutlet UILabel *lblnareshDescription;

@property(strong,nonatomic) IBOutlet UITextView *tvyogeshdescription;
@property(strong,nonatomic) IBOutlet UITextView *tvnareshdescription;

-(IBAction)backBtnPressed:(id)sender;


@end
