//
//  CancleBooking.h
//  JeeTexiApp
//
//  Created by iSquare5 on 12/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface ManagedBookingViewController : UIViewController
{
    IBOutlet UIButton *cancleButton;
    AppDelegate *app;
    IBOutlet UIView *nocancleBookingView;
    IBOutlet UITableView *TableViewCancleBooking;
    MBProgressHUD *hud;
}

/*
@property(strong,nonatomic) IBOutlet UILabel *lblfrom;
@property(strong,nonatomic) IBOutlet UILabel *lblto;
@property(strong,nonatomic) IBOutlet UILabel *lblphoneno;
@property(strong,nonatomic) IBOutlet UILabel *lblbookingid;
@property(strong,nonatomic) IBOutlet UILabel *lbljourney;
@property(strong,nonatomic) IBOutlet UILabel *lblvehicle;
@property(strong,nonatomic) IBOutlet UILabel *lblstatus;
@property(strong,nonatomic) IBOutlet UILabel *lbldrivername;
@property(strong,nonatomic) IBOutlet UILabel *lblpickuptime;
@property(strong,nonatomic) IBOutlet UILabel *lblpickupdate;
@property(strong,nonatomic) IBOutlet UILabel *lbltotalfare;


-(IBAction)cancleBookingBtnPressed:(id)sender;

-(IBAction)getBidBtnPressed:(id)sender;
 */

@end
