//
//  BookingInformationViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 11/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "BookingInformationViewController.h"
#import "MainPageViewController.h"
#import "Reachability.h"
@interface BookingInformationViewController ()

@end

@implementation BookingInformationViewController
@synthesize lbl_carno,lbl_drivername,lbl_journeydate,lbl_journeytime,lbl_totalfare,lbl_vehicle,tv_pickupLocationname,lbl_pickupLocationname,lbl_drivermobileno,lbl_fromcity,lbl_tocity,lbl_bookid;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Scroll.contentSize = CGSizeMake(300, 385);
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadData) userInfo: nil repeats: NO];
    [hud show:YES];
    
}

-(void)loadData
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/get_confirm_order.php",app.urlStr];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\"}",app.useridStr];
        
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",jsonDic);
        
        NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionary];
        for (NSString * key in [jsonDic allKeys])
        {
            if (![[jsonDic objectForKey:key] isKindOfClass:[NSNull class]])
                [jsonDictionary setObject:[jsonDic objectForKey:key] forKey:key];
        }
        
        NSString *successStr = [jsonDic valueForKey:@"Success"];
        NSInteger success = [successStr intValue];
    if (jsonDictionary)
    {
        if (success == 1) {
            NSUserDefaults *prefssave = [NSUserDefaults standardUserDefaults];
            
           // NSArray *RouteJsonDictionary = [prefssave arrayForKey:@"RouteJsonDictionary"];
            
            NSArray *drivernamearr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"d_name"];
            NSString *drivernamestring = [NSString stringWithFormat:@"%@",[drivernamearr objectAtIndex:0]];
            NSArray *drivermobilearr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"d_mobile"];
            NSString *driverphonenostring = [NSString stringWithFormat:@"%@",[drivermobilearr objectAtIndex:0]];
            NSArray *driverpricearr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"price"];
            NSString *driverpricestring =[NSString stringWithFormat:@"%@.00",[driverpricearr objectAtIndex:0]];
            NSArray *bookdatearr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"book_date"];
            NSString  *book_datestring = [NSString stringWithFormat:@"%@",[bookdatearr objectAtIndex:0]];
            NSArray *booktimearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
            NSString  *book_timestring =[NSString stringWithFormat:@"%@",[booktimearr objectAtIndex:0]];
            NSArray *fromcityarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city"];
            NSString  *fromcitystring = [NSString stringWithFormat:@"%@",[fromcityarr objectAtIndex:0]];
            NSArray *fromcityarearr =  [[jsonDictionary valueForKey:@"posts"] valueForKey:@"f_city_area"];
            NSString  *fromcityareastring =[NSString stringWithFormat:@"%@",[fromcityarearr objectAtIndex:0]];
            NSArray *routearr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"r_id"];
            NSString  *routestring = [NSString stringWithFormat:@"%@",[routearr objectAtIndex:0]];
            NSArray *toarr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"t_city"];
            NSString  *tocitystring = [NSString stringWithFormat:@"%@",[toarr objectAtIndex:0]];
            
            NSArray *vahiclearr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"vehicle"];
            NSArray *seatsarr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"seat"];
            
            
            
            lbl_drivername.text = [NSString stringWithFormat:@"%@",drivernamestring];
            lbl_drivermobileno.text = [NSString stringWithFormat:@"%@",driverphonenostring];
            lbl_vehicle.text = [NSString stringWithFormat:@"%@ (%@ PAX)",[vahiclearr objectAtIndex:0],[seatsarr objectAtIndex:0]];
            lbl_journeydate.text = [NSString stringWithFormat:@"%@",book_datestring];
            lbl_journeytime.text = [NSString stringWithFormat:@"%@",book_timestring];
            lbl_fromcity.text = [NSString stringWithFormat:@"%@",fromcitystring];
            lbl_tocity.text = [NSString stringWithFormat:@"%@",tocitystring];
            lbl_pickupLocationname.text = [NSString stringWithFormat:@"%@",fromcityareastring];
            lbl_bookid.text = [NSString stringWithFormat:@"%@",routestring];
            lbl_totalfare.text = [NSString stringWithFormat:@"%@",driverpricestring];
            lbl_pickupLocationname.numberOfLines = 0;
            lbl_pickupLocationname.lineBreakMode = NSLineBreakByWordWrapping;
            
            CGSize maximumLabelSize = CGSizeMake(lbl_pickupLocationname.frame.size.width, CGFLOAT_MAX);
            CGSize expectSize = [lbl_pickupLocationname sizeThatFits:maximumLabelSize];
//            lbl_pickupLocationname.frame = CGRectMake(lbl_pickupLocationname.frame.origin.x, lbl_pickupLocationname.frame.origin.y, expectSize.width, expectSize.height);
//            
//            
//            _lbl_pickuptitletotalfare.frame = CGRectMake(_lbl_pickuptitletotalfare.frame.origin.x, lbl_pickupLocationname.frame.origin.y+expectSize.height+10, _lbl_pickuptitletotalfare.frame.size.width, _lbl_pickuptitletotalfare.frame.size.height);
//            
//            lbl_totalfare.frame = CGRectMake(lbl_totalfare.frame.origin.x,lbl_pickupLocationname.frame.origin.y+expectSize.height+10, lbl_totalfare.frame.size.width, lbl_journeydate.frame.size.height);
        }
   
        }
     
    }
    [hud hide:YES];
}
-(IBAction)OkBtnPressed:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSString *drivernamestring = [prefs stringForKey:@"confirmKey"];
    app.confirmBook=@"yes";
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MainPageViewController *about = (MainPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
    [self.navigationController pushViewController:about animated:YES];
   // [self performSegueWithIdentifier:@"ManagedBookingViewController" sender:self];
}
-(IBAction)backBtnPressed:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MainPageViewController *about = (MainPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
    [self.navigationController pushViewController:about animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
