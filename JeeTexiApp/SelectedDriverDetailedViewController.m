//
//  SelectedDriverDetailedViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "SelectedDriverDetailedViewController.h"
#import "SidebarViewController.h"
#import "ListOfDriverBidViewController.h"
#import "ReviewJeeTextViewController.h"
#import "MainPageViewController.h"
#import "CompletInfoViewController.h"
#import "AsyncImageView.h"
@interface SelectedDriverDetailedViewController ()
@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation SelectedDriverDetailedViewController
@synthesize lbldrivername,lblprice,lblmobileno,lblrating,drivernameStr,driveridStr,driverratingStr,driverpriceStr,drivermobileStr,ratingimage,drivercarnoStr;
@synthesize lbltypetrip,lbltypecar,lblcarno,lblKm,lblcarn,lblpickupdate,lblpickuptime,lbltotal,tvfromcity,tvtocity,carimage,lblfromcity,lbltocity;
@synthesize lbllineagrrementUpper,lbllineDropOffUpper,lbllinePickOnUpper,lbllinePickUpUpper,lbllineTotalUpper,IMGTitleRupee,lblTitleDropOff,lblTitlePickOn,lblTitlePickUp,lblTitleTotal;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Scroll.contentSize = CGSizeMake(320, 400);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/get_select_driver.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"task_id\":\"%@\"}",@"9"];
    
    
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
 
    NSArray *fromcityarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city"];
    NSArray *fromcityareaarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city_area"];
    NSArray *tocityarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"t_city"];
    NSArray *tocityareaarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"t_city_area"];
    NSArray *bookdatearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_date"];
    NSArray *booktimearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
    
    NSArray *pricearr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"price"];
    NSArray *mobilearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"mobile"];
    NSArray *ratarr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"rat"];
    NSArray *routearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"route"];
    NSArray *namearr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"name"];
    NSArray *carnamearr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_name"];
    NSArray *carimagearr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_image"];
    NSArray *seatsarr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_seat"];
    
    NSString *fromcity =  [NSString stringWithFormat:@"%@",[fromcityarr objectAtIndex:0]];
    NSString *fromcityarea = [NSString stringWithFormat:@"%@",[fromcityareaarr objectAtIndex:0]];
    NSString *tocity = [NSString stringWithFormat:@"%@",[tocityarr objectAtIndex:0]];
    NSString *tocityarea = [NSString stringWithFormat:@"%@", [tocityareaarr objectAtIndex:0]];
    NSString *bookdate = [NSString stringWithFormat:@"%@",[bookdatearr objectAtIndex:0 ]];
    NSString *booktime = [NSString stringWithFormat:@"%@",[booktimearr objectAtIndex:0]];
    NSString *price =  [NSString stringWithFormat:@"%@",[pricearr objectAtIndex:0]];
    NSString *mobile = [NSString stringWithFormat:@"%@",[mobilearr objectAtIndex:0]];
    NSString *rat =  [NSString stringWithFormat:@"%@",[ratarr objectAtIndex:0]];
    NSString *route = [NSString stringWithFormat:@"%@",[routearr objectAtIndex:0]];
    NSString *drivername =  [NSString stringWithFormat:@"%@",[namearr objectAtIndex:0]];
    NSString *carname =  [NSString stringWithFormat:@"%@",[carnamearr objectAtIndex:0]];
    NSString *imageurl =  [NSString stringWithFormat:@"%@",[carimagearr objectAtIndex:0]];
    NSString *seats =  [NSString stringWithFormat:@"%@",[seatsarr objectAtIndex:0]];
 //   NSString *fromcity =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
    //  NSString *fromcity =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
    //  NSString *fromcity =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
    
    if ([route isEqualToString:@"one way"]) {
        route = [NSString stringWithFormat:@"One Way"];
    }
    else
    {
        route = [NSString stringWithFormat:@"Round Trip: %@",route];
    }
    
    lblfromcity.text = [NSString stringWithFormat:@"%@,%@",fromcityarea,fromcity];
    lbltocity.text = [NSString stringWithFormat:@"%@,%@",tocityarea,tocity];
    lblpickupdate.text = bookdate;
    lblpickuptime.text = booktime;
    lblprice.text = price;
    lbldrivername.text = drivername;
    lblmobileno.text=mobile;
    lbltypetrip.text = route;
    lbltypecar.text = [NSString stringWithFormat:@"%@ (%@ PAX)",carname,seats];
    carimage.imageURL = [NSURL URLWithString:imageurl];
  /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *drivernamestring = [prefs stringForKey:@"nameKey"];
    NSString *driverphonenostring = [prefs stringForKey:@"mobilenoKey"];
    NSString *driveridstring = [prefs stringForKey:@"driveridKey"];
    NSString *driverpricestring = [NSString stringWithFormat:@"%@.00",[prefs stringForKey:@"driverpriceKey"]];
    NSString *driverreviewstring = [prefs stringForKey:@"driverreviewKey"];
    NSString *drivercarnostring = [prefs stringForKey:@"drivercarnoKey"];
    
    NSArray *RouteJsonData = [prefs arrayForKey:@"RouteJsonDictionary"];
    
    //[prefs setObject:RouteJsonArray forKey:@"RouteJsonDictionary"];
   // NSLog(@"route str__%@",routeidstring);

    if([drivernamestring length] != 0)
    {
        NSLog(@"%ld",(long)drivernamestring);
        drivernameStr = drivernamestring;
        drivermobileStr = driverphonenostring;
        driverpriceStr = driverpricestring;
        driverratingStr =driverreviewstring;
        driveridStr=driveridstring;
        drivercarnoStr = drivercarnostring;
        NSLog(@"%@",lbldrivername);
    }
    //lbldrivername.text =drivernameStr;
    //lblprice.text = driverpriceStr;
    //lblrating.text=driverratingStr;
   // lblmobileno.text=drivermobileStr;
    lblcarno.text=drivercarnoStr;
    */
    checkagreeButton.selected=NO;
  /*
  NSString  *book_datestring = [[RouteJsonData valueForKey:@"book_date"] objectAtIndex:0];
  NSString  *book_timestring = [[RouteJsonData valueForKey:@"book_time"] objectAtIndex:0];
  NSString  *car_imagestring = [[RouteJsonData valueForKey:@"car_image"] objectAtIndex:0];
  NSString  *car_namestring = [[RouteJsonData valueForKey:@"car_name"] objectAtIndex:0];
  NSString  *fromcitystring = [[RouteJsonData valueForKey:@"f_city"] objectAtIndex:0];
  NSString  *fromcityareastring = [[RouteJsonData valueForKey:@"f_city_area"] objectAtIndex:0];
  NSString  *routestring = [[RouteJsonData valueForKey:@"rout"] objectAtIndex:0];
  NSString  *tocitystring = [[RouteJsonData valueForKey:@"t_city"] objectAtIndex:0];
  NSString  *tocityareastring = [[RouteJsonData valueForKey:@"t_city_area"] objectAtIndex:0];
   
    NSString *typetrip;
    if ([routestring isEqualToString:@"one way"]) {
        typetrip = [NSString stringWithFormat:@"One Way"];
    }
    else
    {
        typetrip = [NSString stringWithFormat:@"Round Trip: %@",routestring];
    }
   */
   // lbltypetrip.text = typetrip;
    //lblpickupdate.text = [NSString stringWithFormat:@"%@",book_datestring];
    //lblpickuptime.text = [NSString stringWithFormat:@"%@",book_timestring];
    //lbltypecar.text = [NSString stringWithFormat:@"%@",car_namestring];
 //   carimage.imageURL = [NSURL URLWithString:car_imagestring];
    //lblfromcity.text = [NSString stringWithFormat:@"%@,%@",fromcityareastring,fromcitystring];
    //lbltocity.text = [NSString stringWithFormat:@"%@,%@",tocityareastring,tocitystring];
//    lbltocity.text=@"werwerwerwerwerewrwer werwerwerwerwer r wr wr wr wr w r rw er wer wer we rw erw r wer wer wr we rw r wer wer wer wer wr wrwe rw er wer we rw erw r wer w rw r wer wer w erw er";
   // lblfromcity.text=@"werwerwerwe rw erw er wer we rw er wer w rw erw er wer we rw er we we rw e wr wr wr w er wer wer w e we wr w w  wer wr we rw rwe w er wer we ew ";
    lbltocity.numberOfLines = 0;
    lbltocity.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(lbltocity.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [lbltocity sizeThatFits:maximumLabelSize];
    lbltocity.frame = CGRectMake(lbltocity.frame.origin.x, lbltocity.frame.origin.y, expectSize.width, expectSize.height);
    
    
    
    
    
    lblfromcity.numberOfLines = 0;
    lblfromcity.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize1 = CGSizeMake(lblfromcity.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize1 = [lblfromcity sizeThatFits:maximumLabelSize1];
    lblfromcity.frame = CGRectMake(lblfromcity.frame.origin.x, lblfromcity.frame.origin.y, expectSize1.width, expectSize1.height);
//=======================================
    lbllinePickOnUpper.frame = CGRectMake(lbllinePickOnUpper.frame.origin.x,lblfromcity.frame.origin.y+expectSize1.height+10, lbllinePickOnUpper.frame.size.width, lbllinePickOnUpper.frame.size.height);
    
    lblTitlePickOn.frame = CGRectMake(lblTitlePickOn.frame.origin.x,lbllinePickOnUpper.frame.origin.y+10, lblTitlePickOn.frame.size.width, lblTitlePickOn.frame.size.height);
    
    lblpickupdate.frame = CGRectMake(lblpickupdate.frame.origin.x,lbllinePickOnUpper.frame.origin.y+10, lblpickupdate.frame.size.width, lblpickupdate.frame.size.height);
    
     lblpickuptime.frame = CGRectMake(lblpickuptime.frame.origin.x,lbllinePickOnUpper.frame.origin.y+10, lblpickuptime.frame.size.width, lblpickuptime.frame.size.height);
    
//=======================================
    lbllineDropOffUpper.frame = CGRectMake(lbllineDropOffUpper.frame.origin.x,lblpickuptime.frame.origin.y+lblpickuptime.frame.size.height+10, lbllineDropOffUpper.frame.size.width, lbllineDropOffUpper.frame.size.height);
    
      lblTitleDropOff.frame = CGRectMake(lblTitleDropOff.frame.origin.x,lbllineDropOffUpper.frame.origin.y+10, lblTitleDropOff.frame.size.width, lblTitleDropOff.frame.size.height);
    
    lbltocity.frame = CGRectMake(lbltocity.frame.origin.x,lbllineDropOffUpper.frame.origin.y+10, lbltocity.frame.size.width, lbltocity.frame.size.height);
    
//======================================
    lbllineTotalUpper.frame = CGRectMake(lbllineTotalUpper.frame.origin.x,lbltocity.frame.origin.y+expectSize.height+10, lbllineTotalUpper.frame.size.width, lbllineTotalUpper.frame.size.height);
    
    lblTitleTotal.frame = CGRectMake(lblTitleTotal.frame.origin.x,lbllineTotalUpper.frame.origin.y+10, lblTitleTotal.frame.size.width, lblTitleTotal.frame.size.height);
    
    lbltotal.frame = CGRectMake(lbltotal.frame.origin.x,lbllineTotalUpper.frame.origin.y+10, lbltotal.frame.size.width, lbltotal.frame.size.height);
    
    IMGTitleRupee.frame = CGRectMake(IMGTitleRupee.frame.origin.x,lbllineTotalUpper.frame.origin.y+10, IMGTitleRupee.frame.size.width, IMGTitleRupee.frame.size.height);
 //======================================
    
    lbllineagrrementUpper.frame = CGRectMake(lbllineagrrementUpper.frame.origin.x,IMGTitleRupee.frame.origin.y+ IMGTitleRupee.frame.size.height+10, lbllineagrrementUpper.frame.size.width, lbllineagrrementUpper.frame.size.height);
    
    checkagreeButton.frame = CGRectMake(checkagreeButton.frame.origin.x,lbllineagrrementUpper.frame.origin.y+10, checkagreeButton.frame.size.width, checkagreeButton.frame.size.height);
    
 _lblTitleAgree.frame = CGRectMake(_lblTitleAgree.frame.origin.x,lbllineagrrementUpper.frame.origin.y+10, _lblTitleAgree.frame.size.width, _lblTitleAgree.frame.size.height);
    
    
    
    
    float starCount = [driverratingStr floatValue];
    NSMutableArray *congratulationarr = [[NSMutableArray alloc]init];
    [congratulationarr addObject:[NSString stringWithFormat:@"%@,%@",fromcityarea,fromcity]];
      [congratulationarr addObject:[NSString stringWithFormat:@"%@",bookdate]];
      [congratulationarr addObject:[NSString stringWithFormat:@"%@",booktime]];
      [congratulationarr addObject:[NSString stringWithFormat:@"%@",carname]];
      [congratulationarr addObject:@"1"];
      [congratulationarr addObject:@"350"];
      [congratulationarr addObject:drivername];
    [congratulationarr addObject:mobile];
    [congratulationarr addObject:seats];
    NSUserDefaults *prefssave = [NSUserDefaults standardUserDefaults];
   // NSString *drivernamestring = [prefs stringForKey:@"paylaterKey"];
    [prefssave setObject:congratulationarr forKey:@"congKey"];
    if (starCount == 0 || starCount == 0.5)
    {
        if (starCount == 0)
        {
            ratingimage.image = [UIImage imageNamed:@"1.png"];
        }
        else
        {
            ratingimage.image = [UIImage imageNamed:@"05.png"];
        }
    }
    else if (starCount == 1 || starCount == 1.5)
    {
        if (starCount == 1)
        {
            ratingimage.image = [UIImage imageNamed:@"2.png"];
        }
        else
        {
            ratingimage.image = [UIImage imageNamed:@"15.png"];
        }
    }
    else if (starCount == 2 || starCount == 2.5)
    {
        if (starCount == 2)
        {
            ratingimage.image = [UIImage imageNamed:@"3.png"];
        }
        else
        {
            ratingimage.image = [UIImage imageNamed:@"25.png"];
        }
    }
    else if (starCount == 3 || starCount == 3.5)
    {
        if (starCount == 3)
        {
            ratingimage.image = [UIImage imageNamed:@"4.png"];
        }
        else
        {
            ratingimage.image = [UIImage imageNamed:@"35.png"];
        }
    }
    else if (starCount == 4 || starCount == 4.5)
    {
        if (starCount == 4)
        {
            ratingimage.image = [UIImage imageNamed:@"5.png"];
        }
        else
        {
            ratingimage.image = [UIImage imageNamed:@"45.png"];
        }
    }
    else if (starCount == 5 || starCount == 5.1)
    {
        if (starCount == 5)
        {
            ratingimage.image = [UIImage imageNamed:@"6.png"];
        }
        else
        {
            ratingimage.image = [UIImage imageNamed:@"51.png"];
        }
    }

    
}

-(IBAction)paynowBtnClicked:(id)sender
{
    
}
-(IBAction)paylaterDriverBtnClicked:(id)sender
{
    [self performSegueWithIdentifier:@"BookingInformationViewController" sender:self];
}

- (IBAction)backBtnClicked:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    ListOfDriverBidViewController *about = (ListOfDriverBidViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListOfDriverBidViewController"];
    [self.navigationController pushViewController:about animated:YES];
}

- (IBAction)reviewBtnClicked:(id)sender
{
    [self performSegueWithIdentifier:@"ReviewJeeTextViewController" sender:self];
}
-(IBAction)callBtnClicked:(id)sender
{
    NSString *mobilenostring =drivermobileStr;
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:mobilenostring];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(IBAction)checkagreeBtnClicked:(id)sender
{
    if (checkagreeButton.selected == NO)
    {
        checkagreeButton.selected = YES;
    }
    else
    {
        checkagreeButton.selected=NO;
    }
}

-(IBAction)selectDriverBtnClicked:(id)sender
{
   NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *routeid = [prefs stringForKey:@"idKey"];
    
    NSLog(@"__%@",routeid);
    
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/confirm_order.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"r_id\":\"%@\",\"d_id\":\"%@\",\"u_id\":\"%@\"}",routeid,driveridStr,appDel.useridStr];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    
    NSString *successtr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"Success"] ;
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        CompletInfoViewController *about = (CompletInfoViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"CompletInfoViewController"];
        [self.navigationController pushViewController:about animated:YES];
       // [self performSegueWithIdentifier:@"ListRouteViewController" sender:self];
       // [self clearallField];
        // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Failed!" :@"Failer"];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
