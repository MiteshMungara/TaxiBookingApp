//
//  CompletInfoViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 10/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "AppDelegate.h"

@interface CompletInfoViewController : UIViewController <NIDropDownDelegate>
{
    NIDropDown *dropDown;
    AppDelegate *app;
}

@property(strong,nonatomic) IBOutlet UITextField *txtname;
@property(strong,nonatomic) NSMutableArray *fullnamearr;
@property(strong,nonatomic) IBOutlet UILabel *lbltext;
-(IBAction)backBtnPressed:(id)sender;
@end
