//
//  JourneyViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 08/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "AppDelegate.h"

@interface JourneyViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *app;
    IBOutlet UITableView *TableJouney;
    NSMutableArray *allheadingdetailArry;
    NSMutableArray *alldetailArry;
}

@end
