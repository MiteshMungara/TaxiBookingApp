//
//  ViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 01/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "ViewController.h"
#import "MainPageViewController.h"
#import "Reachability.h"
@interface ViewController ()

@end

@implementation ViewController
@synthesize txtmobileno,txtverificationmobileno;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    verifyView.hidden=YES;
    _lblverify.hidden = YES;
    _lblline.hidden=YES;
    txtverificationmobileno.hidden=YES;
    resendButton.hidden=YES;
    verificationButton.hidden=YES;
   }

#pragma mark - SlideNavigationController Methods -


-(IBAction)SendMobileno:(id)sender
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        [self SendMobileNumber];
    }
}

-(void)SendMobileNumber
{
    //appDel.deviceTokenuser = @"185d17be6c569092bf54037b0fd91df5d024b8fd5bde9b5ed68ebdd69e7c1523";
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/send_otp.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"mobile\":\"%@\",\"token\":\"%@\",\"m_type\":\"%@\"}",txtmobileno.text,appDel.deviceTokenuser,@"ios"];
    NSLog(@"%@",myRequestString);
    
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    
    NSString *successtr = [jsonDictionary valueForKey:@"Success"] ;
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
         //[self alertStatus:@"Success!" :@""];
       //verifyView.hidden=NO;
        _lblverify.hidden = NO;
        _lblline.hidden=NO;
        txtverificationmobileno.hidden=NO;
        resendButton.hidden=NO;
        verificationButton.hidden=NO;
        NSArray *OTParr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"otp"];
        NSString *OTP = [NSString stringWithFormat:@"%@",[OTParr objectAtIndex:0]];
        txtverificationmobileno.text= OTP;
        [txtverificationmobileno becomeFirstResponder];
       // [self clearallField];
        // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Please Enter Mobile Number" :@""];
    }
    
}

-(IBAction)ResendVerificationMobileno:(id)sender
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {

    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/send_otp.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"mobile\":\"%@\",\"token\":\"%@\"}",txtmobileno.text,appDel.deviceTokenuser];
        NSLog(@"%@",myRequestString);
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    
    NSString *successtr = [jsonDictionary valueForKey:@"Success"] ;
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
        //[self alertStatus:@"Success!" :@""];
        verifyView.hidden=NO;
        NSArray *OTParr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"otp"];
        NSString *OTP = [NSString stringWithFormat:@"%@",[OTParr objectAtIndex:0]];
        txtverificationmobileno.text= OTP;
        // [self clearallField];
        // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Please Enter Mobile Number" :@""];
    }
    }
}
-(void)clearallField
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MainPageViewController *about = (MainPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
    [self.navigationController pushViewController:about animated:YES];
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}




-(IBAction)SendVerificationMobileno:(id)sender
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        [self SendVerifiyNumber];
    }
}
-(void)SendVerifiyNumber{
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/check_otp.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myRequestString =[NSString stringWithFormat:@"{\"mobile\":\"%@\",\"otp\":\"%@\"}",txtmobileno.text,txtverificationmobileno.text];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];

    NSString *successtr = [jsonDictionary valueForKey:@"Success"] ;
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
        //[self alertStatus:@"Success!" :@""];
        NSArray *idarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"id"];
        NSString *idstr = [NSString stringWithFormat:@"%@",[idarr objectAtIndex:0]];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:idstr forKey:@"idKey"];
        NSLog(@"__%@",prefs);

       // appDel.useridStr = idstr;
        appDel.useridStr = idstr;
        NSLog(@"%@",appDel.useridStr);
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"bundle: nil];
        MainPageViewController *Main = (MainPageViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
        [self.navigationController pushViewController:Main animated:YES];
        // [self clearallField];
        // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Failed!" :@"Failer"];
    }
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}
- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    
    CGRect frame = self.view.frame;
    CGFloat keyboardHeight = 200.f;
    
    if (up)
    {
        CGRect textFieldFrame = textField.frame;
        CGFloat bottomYPos = textFieldFrame.origin.y + textFieldFrame.size.height;
        
        animateDistance = bottomYPos + 100 + keyboardHeight - frame.size.height;
        if (animateDistance < 0)
            animateDistance = 0;
        else
            animateDistance = fabs(animateDistance);
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    
    if (!(!up && frame.origin.y == 20.f)) {
        if (self.interfaceOrientation == UIDeviceOrientationPortrait)
            frame.origin.y = frame.origin.y + (up ? -animateDistance : animateDistance);
        else if (self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown)
            frame.origin.y = frame.origin.y + (up ? animateDistance : -animateDistance);
        self.view.frame = frame;
    }
    
    [UIView commitAnimations];
}



@end
