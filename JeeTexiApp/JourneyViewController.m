//
//  JourneyViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 08/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "JourneyViewController.h"

@interface JourneyViewController ()

@end

@implementation JourneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    allheadingdetailArry = [[NSMutableArray alloc]init];
    alldetailArry = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allheadingdetailArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ListRouteCell";
    
    UITableViewCell *cell = [TableJouney
                             dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell = nil;
    
    if (cell == nil) {
        cell = [TableJouney dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self performSegueWithIdentifier:@"ListOfDriverBidViewController" sender:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
