//
//  ReviewJeeTextViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 04/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface ReviewJeeTextViewController : UIViewController
{
    AppDelegate *app;
    MBProgressHUD *hud;
    IBOutlet UIButton *Btn_Star_1;
    IBOutlet UIButton *Btn_Star_2;
    IBOutlet UIButton *Btn_Star_3;
    IBOutlet UIButton *Btn_Star_4;
    IBOutlet UIButton *Btn_Star_5;
    IBOutlet UIButton*submitButton;
    IBOutlet UIView *BGView;
}
@property(strong,nonatomic) IBOutlet UILabel *lbl_name;
@property(strong,nonatomic) IBOutlet UILabel *lbl_vahicle;
@property(strong,nonatomic) IBOutlet UILabel *lbl_date;
@property(strong,nonatomic) IBOutlet UILabel *lbl_from;
@property(strong,nonatomic) IBOutlet UILabel *lbl_to;

@property(strong,nonatomic) NSString *fromcityStr;
@property(strong,nonatomic) NSString *tocityStr;
@property(strong,nonatomic) NSString *namedriverStr;
@property(strong,nonatomic) NSString *dateStr;
@property(strong,nonatomic) NSString *vahicleStr;
@property(strong,nonatomic) NSString *timeStr;
@property(strong,nonatomic) NSString *seatsStr;
@property(strong,nonatomic) NSString *routeStr;



@property(strong,nonatomic) IBOutlet UILabel *lbldrivername;
@property (strong,nonatomic) NSString *driveridStr;
@property(strong,nonatomic)NSString *btn_star;
- (IBAction)Btn_Close_Clicked:(id)sender;
- (IBAction)Btn_Star_Clicked:(id)sender;
- (IBAction)Btn_Ok:(id)sender;
@end
