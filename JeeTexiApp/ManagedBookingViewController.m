//
//  ManagedBookingViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 12/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "ManagedBookingViewController.h"
#import "SidebarViewController.h"
#import "ReviewJeeTextViewController.h"
#import "MainPageViewController.h"
#import "Reachability.h"
@interface ManagedBookingViewController ()
{
    NSArray *driver_idarr;
    NSArray *drivernamearr;
    NSArray *drivermobilearr;
    NSArray *driverpricearr;
    NSArray *bookdatearr;
    NSArray *booktimearr;
    NSArray *fromcityarr;
    NSArray *fromcityarearr;
    NSArray *routearr;
    NSArray *toarr;
    NSArray *vahiclearr;
    NSArray *seatsarr;
    NSArray *driverarr;
    NSArray *statusarr;
    NSString *strButton;
    NSArray *journeyarr;
    NSString *didstr;
    NSString *dateString;
    NSString *timeString;
    NSString *fromCityString;
    NSString *toCityString;
    NSString *carString;
    NSString *drivernamestr;
    NSString *seatsstring;
    NSString *ridstring;
    NSDictionary *jsonDictionary;
    
}
@property (nonatomic, retain) SidebarViewController* sidebarVC;
@end

@implementation ManagedBookingViewController
//@synthesize lblbookingid,lbldrivername,lbljourney,lblpickupdate,lblpickuptime,lblstatus,lbltotalfare,lblvehicle,lblphoneno,lblfrom,lblto;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    app = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    CGFloat borderWidth = 1.0f;
    
    /*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [panGesture delaysTouchesBegan];
    [self.view addGestureRecognizer:panGesture];*/
    self.sidebarVC = [[SidebarViewController alloc] init];
    [self.sidebarVC setBgRGB:0x000000];
    self.sidebarVC.view.frame  = self.view.bounds;
    [self.view addSubview:self.sidebarVC.view];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadData) userInfo: nil repeats: NO];
    [hud show:YES];
    
}


-(void)loadData
{
    
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/get_confirm_order.php",app.urlStr];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\"}",app.useridStr];
        
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
        
     
   jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",jsonDictionary);
        
      
       
        NSString *successStr = [jsonDictionary valueForKey:@"Success"];
        NSInteger success = [successStr intValue];
        
    if (success == 1)
    {
        nocancleBookingView.hidden=NO;
  
        drivernamearr =[[jsonDictionary valueForKey:@"posts"]valueForKey:@"d_name"];
        drivermobilearr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"d_mobile"];
        driverpricearr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"price"];
        bookdatearr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"book_date"];
        booktimearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"];
        fromcityarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city"];
        driver_idarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"d_id"];
        fromcityarearr =  [[jsonDictionary valueForKey:@"posts"] valueForKey:@"f_city_area"];
        routearr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"r_id"];
        toarr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"t_city"];
        vahiclearr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"vehicle"];
        seatsarr  = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"seat"];
        statusarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"status"];
        journeyarr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"journey"];
        [TableViewCancleBooking reloadData];
    }
    }
    [hud hide:YES];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return drivernamearr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CancleBookingCell";

    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell = nil;
    
    if (cell == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
    
    if (![[drivernamearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *drivername = (UILabel *)[cell viewWithTag:1];
    drivername.text = [[[jsonDictionary valueForKey:@"posts"]valueForKey:@"d_name"] objectAtIndex:indexPath.row];
    }
    
    if (![[drivermobilearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *mobileno = (UILabel *)[cell viewWithTag:2];
    mobileno.text = [[[jsonDictionary valueForKey:@"posts"] valueForKey:@"d_mobile"] objectAtIndex:indexPath.row];
    }
    
    
    if (![[routearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *bookingid = (UILabel *)[cell viewWithTag:3];
    bookingid.text = [[[jsonDictionary valueForKey:@"posts"] valueForKey:@"r_id"] objectAtIndex:indexPath.row];
    }
    
    if (![[bookdatearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *dateofjourney = (UILabel *)[cell viewWithTag:4];
    dateofjourney.text = [[[jsonDictionary valueForKey:@"posts"] valueForKey:@"book_date"] objectAtIndex:indexPath.row];
    }
    
    
    if (![[vahiclearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
        UILabel *vehicle = (UILabel *)[cell viewWithTag:5];
        vehicle.text = [[[jsonDictionary valueForKey:@"posts"] valueForKey:@"vehicle"] objectAtIndex:indexPath.row];
    }
    else
    {
        UILabel *vehicle = (UILabel *)[cell viewWithTag:5];
        vehicle.text =@"abc";
    }
   
    
    if (![[booktimearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *pickuptime = (UILabel *)[cell viewWithTag:6];
    pickuptime.text = [[[jsonDictionary valueForKey:@"posts"]valueForKey:@"book_time"] objectAtIndex:indexPath.row];
    }
    
    if (![[fromcityarr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *from = (UILabel *)[cell viewWithTag:7];
    from.text = [ [[jsonDictionary valueForKey:@"posts"]valueForKey:@"f_city"] objectAtIndex:indexPath.row];
    }
    if (![[driverpricearr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *totalfare = (UILabel *)[cell viewWithTag:8];
    totalfare.text = [NSString stringWithFormat:@"%@",[[[jsonDictionary valueForKey:@"posts"] valueForKey:@"price"] objectAtIndex:indexPath.row]];
    }
    
    if (![[toarr objectAtIndex:indexPath.row] isEqual:[NSNull null]])
    {
    UILabel *to = (UILabel *)[cell viewWithTag:9];
    to.text = [ [[jsonDictionary valueForKey:@"posts"] valueForKey:@"t_city"] objectAtIndex:indexPath.row];
    }
    
    UILabel *status = (UILabel *)[cell viewWithTag:10];
    NSString *journeystr =[NSString stringWithFormat:@"%@",[[[jsonDictionary valueForKey:@"posts"]valueForKey:@"journey"] objectAtIndex:indexPath.row]];
  
     UIButton *BookingButton = (UIButton *)[cell viewWithTag:11];
    if ([journeystr isEqualToString:@"1"]) {
        [BookingButton setTitle:@"Cancel Booking" forState:UIControlStateNormal];
        status.text = @"Booked!";
        [BookingButton addTarget:self action:@selector(bookThisButton:) forControlEvents:UIControlEventTouchUpInside];
        strButton=@"1";
    }
    else if ([journeystr isEqualToString:@"0"])
    {
        [BookingButton addTarget:self action:@selector(bookThisButton:) forControlEvents:UIControlEventTouchUpInside];
        [BookingButton setTitle:@"Give Rating" forState:UIControlStateNormal];
        status.text = @"Complete!";
        strButton=@"0";
    }
    else
    {
      [BookingButton setTitle:@"No Data" forState:UIControlStateNormal];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)bookThisButton:sender
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
    UIButton *senderButton = (UIButton *)sender;
    UITableViewCell *buttonCell = (UITableViewCell *)senderButton.superview.superview;
    NSIndexPath* pathOfTheCell = [TableViewCancleBooking indexPathForCell:buttonCell];
    NSLog(@"pathOfTheCell.row :%ld",(long)pathOfTheCell.row);
    if ([strButton isEqualToString:@"1"])
    {
        
        app.routeidStr = [routearr objectAtIndex:pathOfTheCell.row];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure want to cancel this taxi?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1;
        [alert show];
        
    }
    else if([strButton isEqualToString:@"0"])
    {
        didstr =[NSString stringWithFormat:@"%@",[driver_idarr objectAtIndex:pathOfTheCell.row]];
        dateString =[NSString stringWithFormat:@"%@",[bookdatearr objectAtIndex:pathOfTheCell.row]];
        timeString =[NSString stringWithFormat:@"%@",[booktimearr objectAtIndex:pathOfTheCell.row]];
        drivernamestr =[NSString stringWithFormat:@"%@",[drivernamearr objectAtIndex:pathOfTheCell.row]];
        carString =[NSString stringWithFormat:@"%@",[vahiclearr objectAtIndex:pathOfTheCell.row]];
        seatsstring = [NSString stringWithFormat:@"%@",[seatsarr objectAtIndex:pathOfTheCell.row]];
        fromCityString = [NSString stringWithFormat:@"%@",[fromcityarr objectAtIndex:pathOfTheCell.row]];
        toCityString = [NSString stringWithFormat:@"%@",[toarr objectAtIndex:pathOfTheCell.row]];
        ridstring = [NSString stringWithFormat:@"%@",[routearr objectAtIndex:pathOfTheCell.row]];
       [self performSegueWithIdentifier:@"ReviewJeeTextViewController" sender:self];
    }
    }
}


- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{

     if(alert.tag == 1)
    {
        if(buttonIndex == alert.cancelButtonIndex)
        {
            
        }
        else
        {
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Process";
            [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                           selector: @selector(cancleBooking) userInfo: nil repeats: NO];
            [hud show:YES];
            
        }
        
    }
    
}
- (IBAction)show:(id)sender
{
    [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
    [self.sidebarVC panDetected:recoginzer];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)cancleBooking
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/cancel_booking.php",app.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
 
    NSString *myRequestString =[NSString stringWithFormat:@"{\"r_id\":\"%@\"}",app.routeidStr];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    NSLog(@"%@",jsonDictionary);
    
    NSString *successtr = [jsonDictionary valueForKey:@"Success"];
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
        // [self alertStatus:@"Success!" :@""];
       
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        MainPageViewController *about = (MainPageViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MainPageViewController"];
        [self.navigationController pushViewController:about animated:YES];
        // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Cancellation of booking failed!" :@"Failer"];
    }
    [hud hide:YES];
    }
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ReviewJeeTextViewController"])
    {
        ReviewJeeTextViewController *vc = [segue destinationViewController];
        vc.driveridStr = didstr;
        vc.namedriverStr = drivernamestr;
        vc.dateStr = dateString;
        vc.timeStr = timeString;
        vc.vahicleStr = carString;
        vc.seatsStr = seatsstring;
        vc.fromcityStr = fromCityString;
        vc.tocityStr=toCityString;
        vc.routeStr = ridstring;
    }
    
}
    


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


