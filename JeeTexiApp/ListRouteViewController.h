//
//  ListRouteViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"
@interface ListRouteViewController : UIViewController
{
    NSMutableArray *fromcityarr;
    NSMutableArray *tocityarr;
    NSMutableArray *datearr;
    NSMutableArray *timearr;
    NSArray *carimagearr;
   // NSMutableArray *fromcityareaarr;
   // NSMutableArray *tocityareaarr;
    NSMutableArray *totalKMarr;
    NSMutableArray *typeTriparr;
    NSMutableArray *daytriparr;
    NSMutableArray *typeofCararr;
    NSMutableArray *routeidarr;
    NSMutableArray *statusarr;
    MBProgressHUD *hud;
    IBOutlet UITableView *TableListRoute;
    AppDelegate *app;
}

-(void)setSelectedButton:(UIButton *)radioButton;
@end
