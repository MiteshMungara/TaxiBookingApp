//
//  AppDelegate.h
//  JeeTexiApp
//
//  Created by iSquare5 on 01/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    AppDelegate *appDel;
}
@property (strong,nonatomic) NSString *confirmBook;
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) NSMutableArray *fulladdressarr;
@property (strong,nonatomic) NSString *opendropdownupper;
@property (strong,nonatomic) NSString *useridStr, *urlStr;
@property (strong,nonatomic) NSString *routeidStr;
@property (strong,nonatomic) NSString *driveridStr;

@property (nonatomic, assign) NSString *deviceToken;
@property (nonatomic, assign) NSString *deviceTokenStr;
@property (nonatomic, strong) NSString *deviceTokenuser;
@property(strong,nonatomic) NSArray *pushNotificationArr;
@property (strong, nonatomic) NSString *openBidStr;


@end

