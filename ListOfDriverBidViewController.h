//
//  ListOfDriverBidViewController.h
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface ListOfDriverBidViewController : UIViewController
{
    IBOutlet UITableView *TableListDriver;
    NSArray *driveridarr;
    NSArray *drivernamearr;
    NSArray *driverpricearr;
    NSArray *driverratingarr;
    NSArray *driverphonenoarr;
    NSArray *drivercarnoarr;
    NSArray *drivercarimgarr;
    NSArray *drivercarseats;
    NSArray *drivercarnamearr;
    AppDelegate *appDel;
    MBProgressHUD *hud;
    IBOutlet UIView *plzwaitmsgView;
}

-(IBAction)removeTaskBtnPressed:(id)sender;
@property(strong,nonatomic) NSString *routeid;
@end
