//
//  ListOfDriverBidViewController.m
//  JeeTexiApp
//
//  Created by iSquare5 on 02/12/15.
//  Copyright © 2015 MitSoft. All rights reserved.
//

#import "ListOfDriverBidViewController.h"
#import "BookingInformationViewController.h"
#import "SidebarViewController.h"
#import "ListRouteViewController.h"
#import "AsyncImageView.h"
#import "Reachability.h"
@interface ListOfDriverBidViewController ()
{
    NSString *drivernamestring;
    NSString *driverphonenostring;
    NSString *driveridstring;
    NSString *driverpricestring;
    NSString *driverratingstring;
    NSString *drivercarnostring;
    NSString *routestring;
    NSArray *poparr;
    NSArray *timearr;
    NSArray *routeidarr;
    UIAlertView *alertView;
    NSTimer *timer;
    NSString *timeovermsg;
    NSMutableArray *cellheightarr;
}

@property (nonatomic, retain) SidebarViewController* sidebarVC;

@end

@implementation ListOfDriverBidViewController
@synthesize routeid;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    /*UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [panGesture delaysTouchesBegan];
    [self.view addGestureRecognizer:panGesture];*/
    self.sidebarVC = [[SidebarViewController alloc] init];
    [self.sidebarVC setBgRGB:0x000000];
    self.sidebarVC.view.frame  = self.view.bounds;
    [self.view addSubview:self.sidebarVC.view];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadData) userInfo: nil repeats: NO];
    [hud show:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RefreshChatTableOFPUSHno:) name:@"RefreshTableOFPUSHnoti" object:nil];
    
}
- (void)RefreshChatTableOFPUSHno:(id)object {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Process";
    [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                   selector: @selector(loadData) userInfo: nil repeats: NO];
    [hud show:YES];
}

- (IBAction)show:(id)sender
{
    [self.sidebarVC showHideSidebar];
}
- (void)panDetected:(UIPanGestureRecognizer*)recoginzer
{
    [self.sidebarVC panDetected:recoginzer];
}

-(void)loadData
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        // Do any additional setup after loading the view.
 
        timeovermsg=@"0";
        NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/list_driver_bid.php",appDel.urlStr];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\"}",appDel.useridStr];//@"52"];
        NSLog(@"User %@",appDel.useridStr);
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",jsonDictionary);
        NSString *succssStr = [jsonDictionary valueForKey:@"Success"];
        NSInteger success = [succssStr intValue];
        if (success == 0)
        {
            plzwaitmsgView.hidden=NO;
         
        }
        else
        {
            
            plzwaitmsgView.hidden=YES;
            TableListDriver.hidden=NO;
            driveridarr  =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"driver_id"];
            drivernamearr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"driver_name"];
            driverpricearr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"bid_price"];
            driverratingarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"rating"];
            driverphonenoarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"mobile"];
           // drivercarnoarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_no"];
            drivercarimgarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_image"];
            drivercarseats =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"seat"];
            drivercarnamearr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_name"];
            timearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"bid_time"];
            poparr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"popup"];
            routeidarr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"route_id"];
            NSString *popMSG = [NSString stringWithFormat:@"%@",[poparr objectAtIndex:0]];
            NSLog(@"POP: %@",popMSG);
            plzwaitmsgView.hidden=YES;
                
        [TableListDriver reloadData];
        }
        
    }
    [hud hide:YES];
    
}

- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the OK/Cancel buttons
    if(alert.tag == 1)
    {
        if(buttonIndex == alert.cancelButtonIndex)
        {
           
            
        }
        else
        {
          
        }
            
    }
   else if(alert.tag == 2)
    {
        if(buttonIndex == alert.cancelButtonIndex)
        {
           
        }
        else
        {
            [self saveBookInformation];
        }
            
    }
    
}
/*
-(void)StopPop
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        
        NSString *urlString = [[NSString alloc]initWithFormat:@"http://taxipapa.com/webservices/stop_popup.php"];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *RoutString =[NSString stringWithFormat:@"%@",[routeidarr objectAtIndex:0]];
        NSString *myRequestString =[NSString stringWithFormat:@"{\"r_id\":\"%@\"}",RoutString];
        NSLog(@"Routes %@",RoutString);
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",jsonDictionary);
        NSString *succssStr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"Success"];
        NSInteger success = [succssStr intValue];
        if (success == 0)
        {
            plzwaitmsgView.hidden=NO;
            //  TableListDriver.hidden=YES;
            //plzwaitmsgView.hidden=NO;
        }
        else
        {
            plzwaitmsgView.hidden=YES;
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Process";
            [hud show:YES];
            [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self
                                           selector: @selector(BookTaxiShow) userInfo: nil repeats: NO];
            
        }
        
    }
    [hud hide:YES];
}

-(void)BookTaxiShow
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        // Do any additional setup after loading the view.
        
        timeovermsg=@"0";
        NSString *urlString = [[NSString alloc]initWithFormat:@"http://taxipapa.com/webservices/list_driver_bid.php"];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *myRequestString =[NSString stringWithFormat:@"{\"u_id\":\"%@\"}",appDel.useridStr];
        NSLog(@"User %@",appDel.useridStr);
        NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
        
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody: requestData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",jsonDictionary);
        NSString *succssStr = [jsonDictionary valueForKey:@"Success"];
        NSInteger success = [succssStr intValue];
        if (success == 0)
        {
            plzwaitmsgView.hidden=NO;
            //  TableListDriver.hidden=YES;
            //plzwaitmsgView.hidden=NO;
        }
        else
        {
            
            plzwaitmsgView.hidden=YES;
            TableListDriver.hidden=NO;
            driveridarr  =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"driver_id"];
            drivernamearr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"driver_name"];
            driverpricearr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"bid_price"];
            driverratingarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"rating"];
            driverphonenoarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"mobile"];
            drivercarnoarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_no"];
            drivercarimgarr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_image"];
            drivercarseats =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"seat"];
            drivercarnamearr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"car_name"];
            timearr = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"bid_time"];
            poparr =  [[jsonDictionary valueForKey:@"posts"]valueForKey:@"popup"];
            routeidarr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"route_id"];
            NSString *popMSG = [NSString stringWithFormat:@"%@",[poparr objectAtIndex:0]];
            NSLog(@"POP: %@",popMSG);
           
            
                [TableListDriver reloadData];
        }
        
    }
    [hud hide:YES];
}*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}



- (IBAction)backBtnClicked:(id)sender
{
   // UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    //ListRouteViewController *about = (ListRouteViewController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ListRouteViewController"];
   // [self.navigationController pushViewController:about animated:YES];
   [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return drivernamearr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"BidCell";
    
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell = nil;
    
    if (cell == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
    cellheightarr = [[NSMutableArray alloc]init];
    UILabel *Drivernametext = (UILabel *)[cell viewWithTag:1];
    Drivernametext.text = [drivernamearr objectAtIndex:indexPath.row];
    
    Drivernametext.numberOfLines = 0;
    Drivernametext.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(Drivernametext.frame.size.width, CGFLOAT_MAX);
    CGSize expectSize = [Drivernametext sizeThatFits:maximumLabelSize];
    Drivernametext.frame = CGRectMake(Drivernametext.frame.origin.x, Drivernametext.frame.origin.y, Drivernametext.frame.size.width, expectSize.height);
    NSString *nameheightStr = [NSString stringWithFormat:@"%f",Drivernametext.frame.size.height];
    float nameheight = [nameheightStr floatValue];
    
    UILabel *Drivernametextdown = (UILabel *)[cell viewWithTag:15];
    Drivernametextdown.frame = CGRectMake(Drivernametextdown.frame.origin.x,Drivernametext.frame.size.height + Drivernametext.frame.origin.y+6, Drivernametextdown.frame.size.width, Drivernametextdown.frame.size.height);
    //--------------------------
    UILabel *Driverpricetext = (UILabel *)[cell viewWithTag:2];
    Driverpricetext.text = [NSString stringWithFormat:@"\u20B9: %@",[driverpricearr objectAtIndex:indexPath.row]];
     Driverpricetext.frame = CGRectMake(Driverpricetext.frame.origin.x,Drivernametextdown.frame.size.height + Drivernametextdown.frame.origin.y+6, Driverpricetext.frame.size.width, Driverpricetext.frame.size.height);
    
    NSString *priceheightStr = [NSString stringWithFormat:@"%f",Driverpricetext.frame.size.height];
    float priceheight = [priceheightStr floatValue];
    
    UILabel *Driverpricetextdown = (UILabel *)[cell viewWithTag:16];
    Driverpricetextdown.frame = CGRectMake(Driverpricetextdown.frame.origin.x,Driverpricetext.frame.size.height + Driverpricetext.frame.origin.y+6, Driverpricetextdown.frame.size.width, Driverpricetextdown.frame.size.height);
  
    //-----------------------------
    
    UILabel *Drivercarnametext = (UILabel *)[cell viewWithTag:4];
    Drivercarnametext.text = [NSString stringWithFormat:@"%@ (%@PAX)",[drivercarnamearr objectAtIndex:indexPath.row],[drivercarseats objectAtIndex:indexPath.row]];;
  
    Drivercarnametext.numberOfLines = 0;
    Drivercarnametext.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSizetypecar = CGSizeMake(Drivercarnametext.frame.size.width, CGFLOAT_MAX);
    CGSize expectSizetypecar = [Drivercarnametext sizeThatFits:maximumLabelSizetypecar];
    Drivercarnametext.frame = CGRectMake(Drivercarnametext.frame.origin.x, Driverpricetextdown.frame.size.height + Driverpricetextdown.frame.origin.y+6, Drivercarnametext.frame.size.width, expectSizetypecar.height);
    NSString *carnameheightStr = [NSString stringWithFormat:@"%f",expectSizetypecar.height];
    float carnameheight = [carnameheightStr floatValue];

        //-------------------------------------------------
    UIImageView *vehicalimage = (UIImageView *)[cell viewWithTag:5];
    vehicalimage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[drivercarimgarr objectAtIndex:indexPath.row]]];
    vehicalimage.layer.borderWidth = 0.8f;
    vehicalimage.layer.borderColor = [UIColor blackColor].CGColor;
    
    
    
    UILabel *Drivercarnametextdown= (UILabel *)[cell viewWithTag:17];
    Drivercarnametextdown.frame = CGRectMake(Drivercarnametextdown.frame.origin.x,Drivercarnametext.frame.size.height + Drivercarnametext.frame.origin.y+6, Drivercarnametextdown.frame.size.width, Drivercarnametextdown.frame.size.height);
    
    NSString *starCountStr = [NSString stringWithFormat:@"%@",[driverratingarr objectAtIndex:indexPath.row]];
    float starCount = [starCountStr floatValue];
    
    //starimage.image = [UIImage imageNamed:@"1.png"];
    //starimage.image = [UIImage imageNamed:@"4.png"];
    UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
    
    if (starCount == 0 || starCount == 0.5)
    {
        if (starCount == 0)
        {
           // UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"1.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
        else
        {
           // UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"05.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
    }
    else if (starCount == 1 || starCount == 1.5)
    {
        if (starCount == 1)
        {
           // UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"2.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
        else
        {
           // UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"15.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
    }
    else if (starCount == 2 || starCount == 2.5)
    {
        if (starCount == 2)
        {
           // UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"3.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
        else
        {
            //UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"25.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
    }
    else if (starCount == 3 || starCount == 3.5)
    {
        if (starCount == 3)
        {
            //UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"4.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
        else
        {
            //UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"35.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
            
        }
    }
    else if (starCount == 4 || starCount == 4.5)
    {
        if (starCount == 4)
        {
           // UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"5.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
        else
        {
            //UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"45.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
    }
    else if (starCount == 5 || starCount == 5.1)
    {
        if (starCount == 5)
        {
            //UIImageView *starimage = (UIImageView *) [cell viewWithTag:3];
            starimage.image = [UIImage imageNamed:@"6.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
        else
        {
            
            starimage.image = [UIImage imageNamed:@"51.png"];
             starimage.frame = CGRectMake(starimage.frame.origin.x,Drivercarnametextdown.frame.size.height + Drivercarnametextdown.frame.origin.y+6, starimage.frame.size.width, starimage.frame.size.height);
        }
    }
   
    
    NSString *btonheightStr;
    float btnheight;
             plzwaitmsgView.hidden=YES;
        UIButton *bookButtom = (UIButton *)[cell viewWithTag:6];
        [bookButtom addTarget:self action:@selector(bookThisButton:) forControlEvents:UIControlEventTouchUpInside];
         bookButtom.frame = CGRectMake(bookButtom.frame.origin.x,starimage.frame.size.height + starimage.frame.origin.y+20, bookButtom.frame.size.width, bookButtom.frame.size.height);
        btonheightStr = [NSString stringWithFormat:@"%f",bookButtom.frame.size.height];
        btnheight = [btonheightStr floatValue];
   
    int addition = (carnameheight) + roundf(priceheight) + roundf(nameheight) + roundf(btnheight);
    [cellheightarr addObject:[NSString stringWithFormat:@"%d",addition]];

    
   
     [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}


#pragma mark --> Height of Tableview Set
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger cellheights;
    NSLog(@"hieths cell %ld",(long)indexPath.row);
    
    if (![[cellheightarr objectAtIndex:0] isEqualToString:@"162"]) {
        NSString *height = [NSString stringWithFormat:@"%@",[cellheightarr objectAtIndex:0]];
        
        cellheights = [height intValue];
        return 162+cellheights-72;
    }
    else
    {
        return 162;
    }
}

-(void)bookThisButton:sender{
    UIButton *senderButton = (UIButton *)sender;
    UITableViewCell *buttonCell = (UITableViewCell *)senderButton.superview.superview;
    NSIndexPath* pathOfTheCell = [TableListDriver indexPathForCell:buttonCell];
    NSLog(@"pathOfTheCell.row :%ld",(long)pathOfTheCell.row);
    driveridstring = [driveridarr objectAtIndex:pathOfTheCell.row];
    routestring = [routeidarr objectAtIndex:pathOfTheCell.row];
    driverpricestring = [driverpricearr objectAtIndex:pathOfTheCell.row];
    appDel.driveridStr = driveridstring;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure want to book this taxi?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 2;
    [alert show];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   /* drivernamestring = [drivernamearr objectAtIndex:indexPath.row];
    driverphonenostring = [driverphonenoarr objectAtIndex:indexPath.row];
    driveridstring = [driveridarr objectAtIndex:indexPath.row];
    driverpricestring = [driverpricearr objectAtIndex:indexPath.row];
    driverratingstring = [driverratingarr objectAtIndex:indexPath.row];
    drivercarnostring = [drivercarnoarr objectAtIndex:indexPath.row];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs setObject:drivernamestring forKey:@"nameKey"];
    [prefs setObject:driverphonenostring forKey:@"mobilenoKey"];
    [prefs setObject:driveridstring forKey:@"driveridKey"];
    [prefs setObject:driverpricestring forKey:@"driverpriceKey"];
    [prefs setObject:driverratingstring forKey:@"driverreviewKey"];
    [prefs setObject:drivercarnostring forKey:@"drivercarnoKey"];
    NSLog(@"__%@",prefs);
    
    [self performSegueWithIdentifier:@"BookingInformationViewController" sender:self];*/
    
}


-(void)saveBookInformation
{
    if ([[Reachability sharedReachability]internetConnectionStatus]==NotReachable)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Check Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
    NSLog(@"%@",appDel.useridStr);
    NSString *urlString = [[NSString alloc]initWithFormat:@"%@webservices/confirm_order.php",appDel.urlStr];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
  
    NSString *myRequestString =[NSString stringWithFormat:@"{\"d_id\":\"%@\",\"r_id\":\"%@\",\"u_id\":\"%@\",\"price\":\"%@\"}",driveridstring,routestring,appDel.useridStr,driverpricestring];
    NSData *requestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: requestData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:nil];
    
    NSLog(@"%@",jsonDictionary);
    NSString *successtr = [[jsonDictionary valueForKey:@"posts"] valueForKey:@"Success"] ;
    // NSString *successStr =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    NSInteger success = [successtr integerValue];
    // NSString *success =[NSString stringWithFormat:@"%@",[succesarr objectAtIndex:0]];
    
    if(success == 2)
    {
        
        [self alertStatus:@"User already exist." :@"Warning!"];
    }
    else if(success == 1)
    {
        // [self alertStatus:@"Success!" :@""];
         appDel.confirmBook = [[jsonDictionary valueForKey:@"posts"]valueForKey:@"confirm_id"];
        
        [self performSegueWithIdentifier:@"BookingInformationViewController" sender:self];
        
         // [self loadproperty];
    }
    else if(success == 0)
    {
        [self alertStatus:@"Bid Selection Failer!" :@"Failer"];
    }
    }
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
